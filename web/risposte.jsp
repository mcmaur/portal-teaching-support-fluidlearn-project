<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="classes.partedinamica.Risposta" %>
<%@page import="classes.Risorsa" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="screen" href="style/style.css"/>
        <script type="text/javascript" src="jscript/Jrisposte.js"></script>
        <title>Risposta page</title>
    </head>
    <body>
        <%@ include file="./common/header.txt"%>
        <%@ include file="./common/nav.txt"%>
        <%@ include file="./common/userInfo.txt"%>
        <article>
            <%
                if(session.getAttribute("username")== null || ((String)session.getAttribute("username")).equals("guest")){
                    RequestDispatcher jspErrore = getServletContext().getRequestDispatcher("/error.jsp");
                    request.setAttribute("errorDescription", "Impossibile effettuare la richiesta. Si prega di accedere al sistema prima");
                    jspErrore.forward(request,response);
                }
            %>
            <div id="elencoRisposte">
                <%
                    String votoMax = (String) request.getAttribute("votoMax");
                    boolean attoreCorpoDocente=false;
                    if(session.getAttribute("redattore").equals("on") || session.getAttribute("esaminatore").equals("on") || session.getAttribute("moderatore").equals("on") || session.getAttribute("docente").equals("on"))
                        attoreCorpoDocente=true;
                    else
                        attoreCorpoDocente=false;
                    boolean isPubblico=false,isDocente=false,AuthorPostEqualsUsername = false;
                    String visRis = (String) request.getAttribute("visRis");
                    if(visRis.equalsIgnoreCase("public"))
                        isPubblico=true;
                    else if(visRis.equals("docente"))
                        isDocente=true;
                    List <Risposta> risList = null;
                    Risorsa risorsa = null;
                    if (request.getAttribute("listRisp") != null){//per la modifica
                        risList = (List <Risposta>) request.getAttribute("listRisp");
                    }
                    if(request.getAttribute("risorsa") != null)
                        risorsa = (Risorsa) request.getAttribute("risorsa");
                    for(int i=0;i<risList.size();i++){
                        AuthorPostEqualsUsername = risList.get(i).getAttore().equals(session.getAttribute("username"));
                        if(isPubblico || ((isDocente) && (AuthorPostEqualsUsername || attoreCorpoDocente))){
                            out.print("<div class='risposta'>\n");
                            out.print("\t\t\tSTUDENTE: <label class='rispostaAutore'>"+risList.get(i).getAttore()+"</label><br/>\n");
                            out.print("\t\t\t<textarea rows='20' cols='45'>"+risList.get(i).getTesto()+"</textarea><br/>\n");
                            if(risList.get(i).getId_risorsa()!=null && risList.get(i).getId_risorsa()!=0){
                                out.print("\t\t\t<label class='rispostaRisorsa'>Risorsa:</label> <br/>\n");
                                out.print("\t\t\t<a class='rispostaRisorsaLink' href='"+risorsa.getUrl()+"'>"+risorsa.getNome()+"</a><br/>\n");
                            }
                            if(attoreCorpoDocente){
                                if(risList.get(i).getId_valutazione()==null || risList.get(i).getId_valutazione()==0){
                                    out.print("\t\t\t<div id='valuta"+risList.get(i).getId()+"'></br>\n");
                                        out.print("\t\t\t\t<input type='button' class='btn' name='valuta' value='Valuta Risposta' onClick='inviaValutazione("+risList.get(i).getId()+");'/></br>\n");
                                        out.print("\t\t\t\t<input type='text' id='voto"+risList.get(i).getId()+"' value='Voto'/> /"+votoMax+"</br>\n");
                                        out.print("\t\t\t\t<input type='hidden' id='id_corso"+risList.get(i).getId()+"' name='id_corso' value='"+request.getAttribute("id_corso")+"'/>\n");
                                        out.print("\t\t\t\t<input type='hidden' id='nome_corso"+risList.get(i).getId()+"' name='nome_corso' value='"+request.getAttribute("nome_corso")+"'/>\n");
                                        out.print("\t\t\t\t<input type='hidden' id='autore"+risList.get(i).getId()+"' value='"+risList.get(i).getAttore()+"'/>\n");
                                        out.print("\t\t\t\t<textarea rows='5' cols='30' id='nota"+risList.get(i).getId()+"'>Inserisci qui le note aggiuntive</textarea></br>\n");
                                    out.print("\t\t\t</div></br>\n");
                                }
                                else
                                    out.print("\t\t\tRisposta già valutata\n");
                            }
                            out.print("\t\t</div>\n");
                        }
                    }
                    out.print("\t</div>\n");
                    out.print("\t<input type='button' value='Esc' onClick='window.history.go(-1);'/>\n");
                %>
        </article>
    </body>
</html>