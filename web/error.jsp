<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="screen" href="style/style.css"/>
        <title>Error Page</title>
    </head>
    <body>
        <%@ include file="./common/header.txt"%>
        <%@ include file="./common/nav.txt"%>
        <%@ include file="./common/userInfo.txt"%>
        <article>
            <img class="dangerPhoto" src="images/various/Segnale_di_pericolo.jpg" alt="Alert Signal"/>
            <%= request.getAttribute("errorDescription")%>
            <% request.removeAttribute("errorDescription");%>
        </article>
    </body>
</html>