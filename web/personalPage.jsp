<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="screen" href="style/style.css"/>
        <script type="text/javascript" src="jscript/JpersonalPage.js"></script>
        <title>Pagina personale</title>
    </head>
    <body>
        <%@ include file="./common/header.txt"%>
        <%@ include file="./common/nav.txt"%>
        <%@ include file="./common/userInfo.txt"%>
        <article>
            <%
                if (session.getAttribute("username") == null || ((String) session.getAttribute("username")).equals("guest")) {
                    RequestDispatcher jspErrore = getServletContext().getRequestDispatcher("/error.jsp");
                    request.setAttribute("errorDescription", "Impossibile effettuare la richiesta. Si prega di accedere al sistema prima");
                    jspErrore.forward(request, response);
                }
            %>
            <script>
                recuperatePersonalInfo();
            </script>
            <div id="tabbernav">
                <div id="btns">
                    <label class="btn" onclick="switchTabber(1);">Info Personali</label>
                    <label class="btn" onclick="switchTabber(2);">Corsi</label>
                    <label class="btn" onclick="switchTabber(3);">Post</label>
                    <label class="btn" onclick="switchTabber(4,<%if (session.getAttribute("redattore").equals("on") || session.getAttribute("esaminatore").equals("on") || session.getAttribute("moderatore").equals("on") || session.getAttribute("docente").equals("on")) {
                            out.println("'doc'");
                        } else {
                            out.println("'stud'");
                        }%>);">Sollecitazioni</label>
                    <label class="btn" onclick="switchTabber(5,<%out.println("'"+session.getAttribute("username")+"'");%>);">Valutazioni</label>
                </div>
                <div id="tabbertab1">
                    <h2>Info personali</h2>
                    <div id="personalInfo">
                        <img id="photo" src="" width="80"/> <br/>
                        Nome:
                        <label id="name"></label> <br/>
                        Cognome:
                        <label id="surname"></label> <br/>
                        Email:
                        <label id="email"></label> <br/>
                        Indirizzo:
                        <label id="address"></label> <br/>
                        Telefono:
                        <label id="telephone"></label> <br/>
                        Anno di prima registrazione:
                        <label id="year_registration"></label> <br/>
                        Carriera:
                        <label id="career_situation"></label> <br/>
                    </div>
                </div>
                <div id="tabbertab2">
                    <h2>Corsi</h2>
                    <div id="tab1c">
                        <select id="select" onchange="requestCoursesListByYear(this.value);">
                            <option value="allYear" selected>Tutti gli anni</option>
                            <option value="2012/2013">2012/2013</option>
                            <option value="2011/2012">2011/2012</option>
                            <option value="2010/2011">2010/2011</option>
                        </select>
                        <div id="inside_tab1c"></div>
                    </div>
                </div>
                <div id="tabbertab3">
                    <h2>Post</h2>
                    <div id="tab3c"></div>
                </div>
                <div id="tabbertab4">
                    <h2>Sollecitazioni</h2>
                    <div id="tab4c"></div>
                </div>
                <div id="tabbertab5">
                    <h2>Valutazioni</h2>
                    <div id="tab5c"></div>
                </div>
            </div>
        </article>
    </body>
</html>
