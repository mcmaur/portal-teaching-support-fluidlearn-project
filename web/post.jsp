<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="classes.partedinamica.Post"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="screen" href="style/style.css"/>
        <title>Post page</title>
    </head>
    <body>
        <%@ include file="./common/header.txt"%>
        <%@ include file="./common/nav.txt"%>
        <%@ include file="./common/userInfo.txt"%>
        <article>
            <%
                if(session.getAttribute("username")== null || ((String)session.getAttribute("username")).equals("guest")){
                    RequestDispatcher jspErrore = getServletContext().getRequestDispatcher("/error.jsp");
                    request.setAttribute("errorDescription", "Impossibile effettuare la richiesta. Si prega di accedere al sistema prima");
                    jspErrore.forward(request,response);
                }
            %>
            <div id='creaNuovoPost'>
                <form action='Controller' method='post'>
                    <%if (request.getAttribute("post") != null) {
                            Post pst = (Post) request.getAttribute("post");
                            out.print("\tTitolo: </br>\n");
                            if (pst.getTitolo() != null) {
                                out.print("\t\t\t<input type='text' class='inputText' name='textTitle' maxlength='30' value='" + pst.getTitolo() + "'/><br/>\n");
                            } else {
                                out.print("\t\t\t<input type='text' class='inputText' maxlength='30' name='textTitle'/><br/>\n");
                            }
                            out.print("\t\t\tTesto: </br>\n");
                            if (pst.getTesto() != null) {
                                out.print("\t\t\t<input type='text' class='inputText' name='textPost' value='" + pst.getTesto() + "'/><br/>\n");
                            } else {
                                out.print("\t\t\t<input type='text' class='inputText' name='textPost'/><br/>\n");
                            }
                            out.print("\t\t\tUDA:&nbsp;</br>\n");
                            if (pst.getId_uda() != null) {
                                out.print("\t\t\t<input type='text' class='inputText' name='uda' value='1394' readonly/><br/>\n");
                            } else {
                                out.print("\t\t\t<input type='text' class='inputText' name='uda' value='1394' readonly/><br/>\n");
                            }
                            out.print("\t\t\tAllegati: &nbsp;</br>\n");
                            if (pst.getId_risorsa() == null) {
                                out.print("\t\t\t<input type='checkbox' name='risorsaAllegata'>Abilita il salvataggio della risorsa<br/>\n");
                            } else {
                                out.print("\t\t\t<input type='checkbox' name='risorsaAllegata' checked>Abilita il salvataggio della risorsa<br/>\n");
                            }
                            out.print("\t\t\t<input type='text' class='inputText' name='alleg' value='23' readonly/><br/>\n");
                            out.print("\t\t\tVisibilità:&nbsp;</br>\n");
                            if (pst.getVisibilita() != null) {
                                out.print("\t\t\t<select class='inputText' name='visibility' >\n");
                                if (pst.getVisibilita().equals("public")) {
                                    out.print("\t\t\t\t<option value='public' selected>Visibile a tutti i partecipanti</option>\n");
                                } else {
                                    out.print("\t\t\t\t<option value='public'>Visibile a tutti i partecipanti</option>\n");
                                }
                                if (pst.getVisibilita().equals("docente")) {
                                    out.print("\t\t\t\t<option value='docente' selected>Visibile solo al corpo docente</option>\n");
                                } else {
                                    out.print("\t\t\t\t<option value='docente'>Visibile solo al corpo docente</option>\n");
                                }
                                if (pst.getVisibilita().equals("private")) {
                                    out.print("\t\t\t\t<option value='private' selected>Solo autore </option>\n");
                                } else {
                                    out.print("\t\t\t\t<option value='private'>Solo autore </option>\n");
                                }
                                out.print("\t\t\t</select><br/>\n");
                            } else {
                                out.print("\t\t\t<select class='inputText' name='visibility' >\n");
                                out.print("\t\t\t\t<option value='public'>Visibile a tutti i partecipanti</option>\n");
                                out.print("\t\t\t\t<option value='docente'>Visibile solo al corpo docente</option>\n");
                                out.print("\t\t\t\t<option value='private'>Solo autore </option>\n");
                                out.print("\t\t\t</select><br/>\n");
                            }
                            if (pst.isIsBozza() == false) {
                                out.print("\t\t\t<input type='checkbox' name='isDraft'>Crea come bozza</input><br/>\n");
                            } else {
                                out.print("\t\t\t<input type='checkbox' name='isDraft' checked>Crea come bozza</input><br/>\n");
                            }
                            if (request.getAttribute("course_name") != null) {
                                out.print("\t\t\t<input type='hidden' id='type' name='type' value='aggiungiPost'/>\n");
                                out.print("\t\t\t<input type='hidden' name='course_name' value='" + request.getAttribute("course_name") + "'/>\n");
                            } else {
                                out.print("\t\t\t<input type='hidden' id='type' name='type' value='salvaPostModificato'/>\n");
                                out.print("\t\t\t<input type='hidden' id='type' name='post_id' value='" + pst.getId() + "'/>\n");
                                out.print("\t\t\t<input type='hidden' name='course_name' value='" + pst.getNome_corso() + "'/>\n");
                            }
                            if (request.getAttribute("course_id") != null) {
                                out.print("\t\t\t<input type='hidden' name='course_id' value='" + request.getAttribute("course_id") + "'/>\n");
                            } else {
                                out.print("\t\t\t<input type='hidden' name='course_id' value='" + pst.getId_corso() + "'/>\n");
                            }
                            out.print("\t\t\t<input type='hidden' name='plugin' value='no'/>\n");
                            out.print("\t\t\t<input type='submit' value='Ok'/>\n");
                        }
                        out.print("\t\t\t<input type='button' value='Esc' onClick='window.history.go(-1);'/>\n");%>
                </form>
            </div>
        </article>
    </body>
</html>