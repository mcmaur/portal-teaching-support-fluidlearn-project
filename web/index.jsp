<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="screen" href="style/style.css"/>
        <script type="text/javascript">
            var xhrObj = setXMLHttpRequest(); // crea oggetto XMLHTTPRequest
            function setXMLHttpRequest() {
                var xhr = null;
                if (window.XMLHttpRequest) {
                    xhr = new XMLHttpRequest(); // browser standard con supporto nativo
                }
                else if (window.ActiveXObject) { // browser MS Internet Explorer - ActiveX
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                }
                return xhr;
            }

            function setPermission(act, val) {
                var parameter = "type=setPermission&actor=" + act + "&value=" + val;
                xhrObj.onreadystatechange = relaod;
                xhrObj.open("POST", "Controller", true);
                xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
                xhrObj.send(parameter);
            }

            function relaod() {//reload the page
                if (xhrObj.readyState === 4 && xhrObj.status === 200)
                    document.location.reload(true);
            }
        </script>
        <title>Benvenuto su FluidLearn</title>
    </head>
    <body>
        <%@ include file="./common/header.txt"%>
        <%@ include file="./common/nav.txt"%>
        <%@ include file="./common/userInfo.txt"%>
        <article>
            BENVENUTO IN FLUID LEARN !! <% out.print("<br/>\n");
                out.print("\t\t--settedPermissions--<br/>\n");
                String[] s = session.getValueNames();
                for (int i = 0; i < s.length; i++) {
                    out.print("\t\t\t" + s[i] + " : ");
                    out.print(session.getAttribute(s[i]) + "<br/>\n");
                }
                out.print("\t\t--/settedPermissions--<br/>\n");

                if (session.getAttribute("studente") != null) {
                    if (((String) session.getAttribute("studente")).equals("on")) {
                        out.print("\t\t<input type='checkbox' name='studente' value='studente' onclick='setPermission(\"studente\",\"off\");' checked>studente\n");
                    } else {
                        out.print("\t\t<input type='checkbox' name='studente' value='studente' onclick='setPermission(\"studente\",\"on\");'>studente\n");
                    }
                }
                if (session.getAttribute("redattore") != null) {
                    if (((String) session.getAttribute("redattore")).equals("on")) {
                        out.print("\t\t<input type='checkbox' name='studente' value='redattore' onclick='setPermission(\"redattore\",\"off\");' checked>redattore\n");
                    } else {
                        out.print("\t\t<input type='checkbox' name='studente' value='redattore' onclick='setPermission(\"redattore\",\"on\");'>redattore\n");
                    }
                }
                if (session.getAttribute("esaminatore") != null) {
                    if (((String) session.getAttribute("esaminatore")).equals("on")) {
                        out.print("\t\t<input type='checkbox' name='studente' value='redattore' onclick='setPermission(\"esaminatore\",\"off\");' checked>esaminatore\n");
                    } else {
                        out.print("\t\t<input type='checkbox' name='studente' value='redattore' onclick='setPermission(\"esaminatore\",\"on\");'>esaminatore\n");
                    }
                }
                if (session.getAttribute("moderatore") != null) {
                    if (((String) session.getAttribute("moderatore")).equals("on")) {
                        out.print("\t\t<input type='checkbox' name='studente' value='moderatore' onclick='setPermission(\"moderatore\",\"off\");' checked>moderatore\n");
                    } else {
                        out.print("\t\t<input type='checkbox' name='studente' value='moderatore' onclick='setPermission(\"moderatore\",\"on\");'>moderatore\n");
                    }
                }
                if (session.getAttribute("docente") != null) {
                    if (((String) session.getAttribute("docente")).equals("on")) {
                        out.print("\t\t<input type='checkbox' name='studente' value='docente' onclick='setPermission(\"docente\",\"off\");' checked>docente");
                    } else {
                        out.print("\t\t<input type='checkbox' name='studente' value='moderatore' onclick='setPermission(\"docente\",\"on\");'>docente");
                    }
                }
            %>
        </article>
        <%@ include file="./common/footer.txt"%>
    </body>
</html>