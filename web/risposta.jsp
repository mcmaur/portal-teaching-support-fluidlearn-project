<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="classes.partedinamica.Risposta" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="screen" href="style/style.css"/>
        <title>Risposta page</title>
    </head>
    <body>
        <%@ include file="./common/header.txt"%>
        <%@ include file="./common/nav.txt"%>
        <%@ include file="./common/userInfo.txt"%>
        <article>
            <%
                if(session.getAttribute("username")== null || ((String)session.getAttribute("username")).equals("guest")){
                    RequestDispatcher jspErrore = getServletContext().getRequestDispatcher("/error.jsp");
                    request.setAttribute("errorDescription", "Impossibile effettuare la richiesta. Si prega di accedere al sistema prima");
                    jspErrore.forward(request,response);
                }
            %>
            <div id="creaNuovaSollecitazione">
                <form action='Controller' method='post'>
                    <%
                        Risposta ris = null;
                        if (request.getAttribute("rispostaNuova") != null) {//per la modifica
                            ris = (Risposta) request.getAttribute("rispostaNuova");
                        }
                        out.print("\t\t\tTesto: </br>\n");
                        if (ris.getTesto() != null) {
                            out.print("\t\t\t<input type='text' class='inputText' name='text' value='" + ris.getTesto() + "'/><br/>\n");
                        } else {
                            //out.print("\t\t\t<input type='text' class='inputText' name='text'/><br/>\n");
                            out.print("\t\t\t<textarea class='inputText' name='text' rows='20' cols='45'></textarea><br/>\n");
                        }
                        out.print("\t\t\tAllegati: &nbsp;</br>\n");
                        if (ris.getId_risorsa() == null) {
                            out.print("\t\t\t<input type='checkbox' name='risorsaAllegata'>Abilita il salvataggio della risorsa<br/>\n");
                        } else {
                            out.print("\t\t\t<input type='checkbox' name='risorsaAllegata' checked>Abilita il salvataggio della risorsa<br/>\n");
                        }
                        out.print("\t\t\t<input type='text' class='inputText' name='alleg' value='23' readonly/><br/>\n");
                        /*------------------------------------Hidden Field-----------------------------------------------------------*/
                        out.print("\t\t\t<input type='hidden' id='type' name='type' value='aggiungiRisposta'/>\n");
                        out.print("\t\t\t<input type='hidden' name='soll_id' value='" + ris.getId_contributo() + "'/>\n");
                        out.print("\t\t\t<input type='hidden' name='autore' value='" + ris.getAttore() + "'/>\n");
                        /*------------------------------------------------------------------------------------------------------*/
                        out.print("\t\t\t<input type='submit' value='Ok'/>\n");
                        out.print("\t\t\t<input type='button' value='Esc' onClick='window.history.go(-1);'/>\n");
                    %>
                </form>
            </div>
        </article>
    </body>
</html>