var permission;//doc per docente; stud per studente
var user;

//==================================================setting for ajax
var xhrObj = setXMLHttpRequest(); // crea oggetto XMLHTTPRequest

function setXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest(); // browser standard con supporto nativo
    }
    else if (window.ActiveXObject) { // browser MS Internet Explorer - ActiveX
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xhr;
}
//==================================================setting the other id invisible onclick

function switchTabber(numeroTab, option) {
    switch (numeroTab) {
        case 1:
            document.getElementById("tabbertab1").style.display = 'block';
            document.getElementById("tabbertab2").style.display = 'none';
            document.getElementById("tabbertab3").style.display = 'none';
            document.getElementById("tabbertab4").style.display = 'none';
            document.getElementById("tabbertab5").style.display = 'none';
            recuperatePersonalInfo();
            break;
        case 2:
            document.getElementById("tabbertab1").style.display = 'none';
            document.getElementById("tabbertab2").style.display = 'block';
            document.getElementById("tabbertab3").style.display = 'none';
            document.getElementById("tabbertab4").style.display = 'none';
            document.getElementById("tabbertab5").style.display = 'none';
            requestCoursesList();
            break;
        case 3:
            document.getElementById("tabbertab1").style.display = 'none';
            document.getElementById("tabbertab2").style.display = 'none';
            document.getElementById("tabbertab3").style.display = 'block';
            document.getElementById("tabbertab4").style.display = 'none';
            document.getElementById("tabbertab5").style.display = 'none';
            listPostByUserName();
            break;
        case 4:
            permission=option;
            document.getElementById("tabbertab1").style.display = 'none';
            document.getElementById("tabbertab2").style.display = 'none';
            document.getElementById("tabbertab3").style.display = 'none';
            document.getElementById("tabbertab4").style.display = 'block';
            document.getElementById("tabbertab5").style.display = 'none';
            listSollecitazioni();
            break;
        case 5:
            user=option;
            document.getElementById("tabbertab1").style.display = 'none';
            document.getElementById("tabbertab2").style.display = 'none';
            document.getElementById("tabbertab3").style.display = 'none';
            document.getElementById("tabbertab4").style.display = 'none';
            document.getElementById("tabbertab5").style.display = 'block';
            listValutazioni();
            break;
    }
}
//==================================================tab nr1 personal user info
function recuperatePersonalInfo() {
    var parameter = "type=personalInfo";
    xhrObj.onreadystatechange = updatePersonalInfo;
    xhrObj.open("POST", "Controller", true);
    xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
    xhrObj.send(parameter);
}

function updatePersonalInfo() {
    if (xhrObj.readyState === 4 && xhrObj.status === 200)
    {
        var risp = JSON.parse(xhrObj.responseText);
        document.getElementById("photo").src = risp.photo_path;
        document.getElementById("name").innerHTML = risp.name;
        document.getElementById("surname").innerHTML = risp.surname;
        document.getElementById("address").innerHTML = risp.address;
        document.getElementById("email").innerHTML = risp.email;
        document.getElementById("telephone").innerHTML = risp.telephone;
        document.getElementById("year_registration").innerHTML = risp.year_registration;
        if (risp.career_situation === "ACTIVE")
            document.getElementById("career_situation").innerHTML = "Attiva";
        else
            document.getElementById("career_situation").innerHTML = "Congelata";
    }
}

//==================================================tab nr2 list of courses
function requestCoursesList() {
    var parameter = "type=listAllCourses";
    xhrObj.onreadystatechange = updateCoursesList;
    xhrObj.open("POST", "Controller", true);
    xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
    xhrObj.send(parameter);
}

function updateCoursesList() {
    if (xhrObj.readyState === 4 && xhrObj.status === 200)
    {
        var risp = JSON.parse(xhrObj.responseText);
        document.getElementById("inside_tab1c").innerHTML = " ";
        for (i = 0; i < risp.length; i++)
            document.getElementById("inside_tab1c").innerHTML += "<img id='small_arrow' src='images/various/freccetta.png'/>&nbsp;&nbsp;"
                    + "<a class='course_title' href='Controller?type=showCourse&CourseName=" + risp[i].name + "'>" + risp[i].name + "</a>\n"
                    + "<label class='date'>" + risp[i].years + "</label> <br/>"
                    + "<label class='course_description'>" + risp[i].description + "</label> <br/>";
    }
}
//==================================================
function requestCoursesListByYear(year) {
    if (year === "allYear")
        requestCoursesList();
    else {
        var parameter = "type=listCoursesByYear&year=" + year;
        xhrObj.onreadystatechange = updateCoursesListByYear;
        xhrObj.open("POST", "Controller", true);
        xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
        xhrObj.send(parameter);
    }
}

function updateCoursesListByYear() {
    if (xhrObj.readyState === 4 && xhrObj.status === 200) {
        var risp = JSON.parse(xhrObj.responseText);
        document.getElementById("inside_tab1c").innerHTML = " ";
        for (i = 0; i < risp.length; i++)
            document.getElementById("inside_tab1c").innerHTML += "<img id='small_arrow' src='images/various/freccetta.png'/>&nbsp;&nbsp;"
                    + "<a class='course_title' href='Controller?type=showCourse&CourseName=" + risp[i].name + "'>" + risp[i].name + "</a> <br/>"
                    + "<label class='course_description'>" + risp[i].description + "</label> <br/>";
    }
}

//==================================================tab nr3 list of user's post
function listPostByUserName() {
    var parameter = "type=listPostByUserName";
    xhrObj.onreadystatechange = updateListPostByUserName;
    xhrObj.open("POST", "Controller", true);
    xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
    xhrObj.send(parameter);
}

function updateListPostByUserName() {
    if (xhrObj.readyState === 4 && xhrObj.status === 200)
    {
        var risp = JSON.parse(xhrObj.responseText);
        document.getElementById("tab3c").innerHTML = "";
        for (i = 0; i < risp.length; i++) {
            if (risp[i].isBozza === false)
                document.getElementById("tab3c").innerHTML += "<img id='small_arrow' src='images/various/freccetta.png'/>&nbsp;&nbsp;"
                        + "<a class='course_title' href='Controller?type=showCourse&CourseName=" + risp[i].nome_corso + "'>" + risp[i].nome_corso + "</a> <br/>"
                        + "<label class='post_text'>\"" + risp[i].testo + "\"</label> <br/>";
            else
                document.getElementById("tab3c").innerHTML += "<img id='small_arrow' src='images/various/freccetta.png'/>&nbsp;&nbsp;"
                        + "<a class='course_title' href='Controller?type=showCourse&CourseName=" + risp[i].nome_corso + "'>" + risp[i].nome_corso + "</a> <br/>"
                        + "<label class='post_text_draft'>\"" + risp[i].testo + "\"</label> <br/>";
        }
    }
}

//==================================================tab nr4 list of sollecitazioni
function listSollecitazioni() {
    var parameter = "type=listAllSollecitazioni";
    xhrObj.onreadystatechange = updateListSollecitazioni;
    xhrObj.open("POST", "Controller", true);
    xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
    xhrObj.send(parameter);
}

function updateListSollecitazioni() {
    if (xhrObj.readyState === 4 && xhrObj.status === 200) {
        var risp = JSON.parse(xhrObj.responseText);
        document.getElementById("tab4c").innerHTML = "";
        for (i = 0; i < risp.length; i++) {
            if (risp[i].isBozza === false)
                document.getElementById("tab4c").innerHTML += "<img id='small_arrow' src='images/various/freccetta.png'/>&nbsp;&nbsp;"
                        + "<a class='course_title' href='Controller?type=showCourse&CourseName=" + risp[i].nome_corso + "'>" + risp[i].nome_corso + "</a> <br/>"
                        + "<label class='post_text'>\"" + risp[i].testo + "\"</label> <br/>";
            else {
                if(permission==="doc")
                    document.getElementById("tab4c").innerHTML += "<img id='small_arrow' src='images/various/freccetta.png'/>&nbsp;&nbsp;"
                        + "<a class='course_title' href='Controller?type=showCourse&CourseName=" + risp[i].nome_corso + "'>" + risp[i].nome_corso + "</a> <br/>"
                        + "<label class='post_text_draft'>\"" + risp[i].testo + "\"</label> <br/>";
            }
        }
    }
}

//==================================================tab nr4 list of sollecitazioni
function listValutazioni() {
    var parameter = "type=listValutazioni";
    xhrObj.onreadystatechange = updateListValutazioni;
    xhrObj.open("POST", "Controller", true);
    xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
    xhrObj.send(parameter);
}

function updateListValutazioni() {
    if (xhrObj.readyState === 4 && xhrObj.status === 200) {
        var risp = JSON.parse(xhrObj.responseText);
        document.getElementById("tab5c").innerHTML = "";
        for (i = 0; i < risp.length; i++) {
            if(risp[i].autore_risp===user){
                document.getElementById("tab5c").innerHTML += "<img id='small_arrow' src='images/various/freccetta.png'/>&nbsp;&nbsp;"
                        + "<label class='val_esaminatore'>" + risp[i].nome_corso + "</label></a>"
                        + "<label class='val_voto'>  "+ risp[i].voto +"  </label> <br/>"
                        + "<label class='val_description'>\"" + risp[i].nota + "\"</label> <br/>";
            }
        }
    }
}