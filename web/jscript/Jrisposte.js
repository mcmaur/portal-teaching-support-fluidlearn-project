//==================================================setting for ajax
var xhrObj = setXMLHttpRequest(); // crea oggetto XMLHTTPRequest

function setXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest(); // browser standard con supporto nativo
    }
    else if (window.ActiveXObject) { // browser MS Internet Explorer - ActiveX
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xhr;
}

//==================================================
var divId;
function inviaValutazione(risp_id){
    var answer = confirm("Sei sicuro di voler assegnare tale voto?");
    if (answer){
        divId="valuta"+risp_id;
        var voto = document.getElementById("voto"+risp_id).value;
        var autore = document.getElementById("autore"+risp_id).value;
        var nota = document.getElementById("nota"+risp_id).value;
        var id_corso = document.getElementById("id_corso"+risp_id).value;
        var nome_corso = document.getElementById("nome_corso"+risp_id).value;
        var parameter = "type=salvaValutazione&id_risposta="+risp_id+"&voto="+voto+"&autore="+autore+"&nota="+nota+"&id_corso="+id_corso+"&nome_corso="+nome_corso;
        xhrObj.onreadystatechange = segnalaFattoInValutazione;
        xhrObj.open("POST", "Controller", true);
        xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
        xhrObj.send(parameter);
    }
}

function segnalaFattoInValutazione() {
    if (xhrObj.readyState === 4 && xhrObj.status === 200)
    {
        document.getElementById(divId).innerHTML='Eseguito';
        divId=null;
    }
}