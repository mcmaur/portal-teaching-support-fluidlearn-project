//==================================================setting for ajax
var xhrObj = setXMLHttpRequest(); // crea oggetto XMLHTTPRequest

function setXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest(); // browser standard con supporto nativo
    }
    else if (window.ActiveXObject) { // browser MS Internet Explorer - ActiveX
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xhr;
}

//==================================================for adding new comment
function addNewComment(text, post_id) {
    if (text !== "") {
        var parameter = "type=newComment&post_id=" + post_id + "&text=" + text + "&risorsa=0";
        xhrObj.onreadystatechange = relaod;
        xhrObj.open("POST", "Controller", true);
        xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
        xhrObj.send(parameter);
    }
}

function relaod() {//reload the page
    if (xhrObj.readyState === 4 && xhrObj.status === 200)
        document.location.reload(true);
}

//==================================================for deleting a post
function deletePost(post_id) {
    var answer = confirm("Sei sicuro di voler cancellare il post?");
    if (answer) {
        var parameter = "type=deletePost&post_id=" + post_id;
        xhrObj.onreadystatechange = relaod;
        xhrObj.open("POST", "Controller", true);
        xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
        xhrObj.send(parameter);
    }
}

//==================================================for saving a draft
function saveDraft(post_id) {
    var parameter = "type=saveDraft&post_id=" + post_id;
    xhrObj.onreadystatechange = relaod;
    xhrObj.open("POST", "Controller", true);
    xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
    xhrObj.send(parameter);
}

//===================================================show file picker
function showFilePicker(FilePickerInput) {
    if (FilePickerInput.style.display === 'none')
        FilePickerInput.style.display = 'block';
    else
        FilePickerInput.style.display = 'none';
}

//==================================================richiedo di trasformare il post di id post_id in bozza
function returnToDraft(post_id) {
        var parameter = "type=returnToDraft&post_id=" + post_id;
        xhrObj.onreadystatechange = relaod;
        xhrObj.open("POST", "Controller", true);
        xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
        xhrObj.send(parameter);
}

//==================================================for deleting a comment
function eliminaCommento(commento_id) {
    var answer = confirm("Sei sicuro di voler cancellare il commento?");
    if (answer) {
        var parameter = "type=deleteCommento&commento_id=" + commento_id;
        xhrObj.onreadystatechange = relaod;
        xhrObj.open("POST", "Controller", true);
        xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
        xhrObj.send(parameter);
    }
}

//==================================================for deleting a comment
function eliminaSollecitazione(soll_id) {
    var answer = confirm("Sei sicuro di voler cancellare la sollecitazione?");
    if (answer) {
        var parameter = "type=deleteSollecitazione&sollecitazione_id=" + soll_id;
        xhrObj.onreadystatechange = relaod;
        xhrObj.open("POST", "Controller", true);
        xhrObj.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"); //Send the proper header information along with the request
        xhrObj.send(parameter);
    }
}