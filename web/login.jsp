<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="screen" href="style/style.css"/>
        <title>Login page</title>
    </head>
    <body>
        <%@ include file="./common/header.txt"%>
        <%@ include file="./common/nav.txt"%>
        <%@ include file="./common/userInfo.txt"%>
        <article>
            <div id="log-reg_form">
                <form action="Controller" method="post">
                    Username:
                    <input type="text" id="nome" name="username"><br/>
                    Password:&nbsp;
                    <input type="password" id="passw" name="password"><br/>
                    <input type="hidden"   id="type" name="type" value="login">
                    <input type="submit" value="Ok">
                </form>
            </div>
        </article>
    </body>
</html>