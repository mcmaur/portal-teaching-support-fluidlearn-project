<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="classes.partedinamica.Post" %>
<%@page import="classes.partedinamica.Commento" %>
<%@page import="classes.partedinamica.Sollecitazione" %>
<%@page import="classes.Risorsa" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="screen" href="style/style.css"/>
        <script type="text/javascript" src="jscript/Jcourse.js"></script>
        <title>Corso</title>
    </head>
    
    <body>
        <%@ include file="./common/header.txt"%>
        
        <%@ include file="./common/nav.txt"%>
        
        <%@ include file="./common/userInfo.txt"%>
        
        <article>
            <%
                if(session.getAttribute("username")== null || ((String)session.getAttribute("username")).equals("guest")){
                    RequestDispatcher jspErrore = getServletContext().getRequestDispatcher("/error.jsp");
                    request.setAttribute("errorDescription", "Impossibile effettuare la richiesta. Si prega di accedere al sistema prima");
                    jspErrore.forward(request,response);
                }
            %>
            <%
                //---------------------------------------------------------------------- controllo se possiede i permessi da docente
                boolean attoreCorpoDocente=false;
                if(session.getAttribute("redattore").equals("on") || session.getAttribute("esaminatore").equals("on") || session.getAttribute("moderatore").equals("on") || session.getAttribute("docente").equals("on")){
                    attoreCorpoDocente=true;
                }else{
                    attoreCorpoDocente=false;
                }
                //---------------------------------------------------------------------- scrivo titolo e descrizione del corso
                out.print("\t<div class='course_title' style='text-align:center;'>\n");
                if(request.getAttribute("CourseName") != null)
                    out.print("\t\t\t"+request.getAttribute("CourseName")+"<br/>\n");
                out.print("\t\t</div>\n");
                out.print("\t\t<div class='course_description'>\n");
                if(request.getAttribute("CourseDescription") != null)
                    out.print("\t\t\t"+request.getAttribute("CourseDescription")+"\n");
                out.print("\t\t</div>\n\n");
                //---------------------------------------------------------------------- bottoni per creare nuovo post
                out.print("\t\t<form action='Controller' method='post'>\n");//senza plugin
                    out.print("\t\t\t<input type='hidden' id='type' name='type' value='newPost'>\n");
                    out.print("\t\t\t<input type='hidden' name='course_name' value='"+request.getAttribute("CourseName")+"'>\n");
                    out.print("\t\t\t<input type='hidden' name='course_id' value='"+request.getAttribute("CourseID")+"'>\n");
                    out.print("\t\t\t<input type='hidden' name='plugin' value='no'>\n");
                    out.print("\t\t\t<input type='submit' value='crea nuovo post'>\n");
                out.print("\t\t</form>\n\n");
                out.print("\t\t<form action='Controller' method='post'>\n");//con plugin
                    out.print("\t\t\t<input type='hidden' id='type' name='type' value='newPost'>\n");
                    out.print("\t\t\t<input type='hidden' name='plugin' value='yes'>\n");
                    out.print("\t\t\t<input type='submit' value='crea nuovo post con plugin'>\n");
                out.print("\t\t</form>\n\n");
                //----------------------------------------------------------------------bottoni per creare nuova sollecitazione
                if(attoreCorpoDocente){
                    out.print("\t\t<form action='Controller' method='post'>\n");//senza plugin
                        out.print("\t\t\t<input type='hidden' id='type' name='type' value='newSollecitazione'>\n");
                        out.print("\t\t\t<input type='hidden' name='course_name' value='"+request.getAttribute("CourseName")+"'>\n");
                        out.print("\t\t\t<input type='hidden' name='course_id' value='"+request.getAttribute("CourseID")+"'>\n");
                        out.print("\t\t\t<input type='hidden' name='plugin' value='no'>\n");
                        out.print("\t\t\t<input type='submit' value='crea nuova sollecitazione'>\n");
                    out.print("\t\t</form>\n\n");
                    
                    out.print("\t\t<form action='Controller' method='post'>\n");//con plugin
                        out.print("\t\t\t<input type='hidden' id='type' name='type' value='newSollecitazione'>\n");
                        out.print("\t\t\t<input type='hidden' name='plugin' value='yes'>\n");
                        out.print("\t\t\t<input type='submit' value='crea nuova sollecitazione con plugin'>\n");
                    out.print("\t\t</form>\n\n");
                }
                //---------------------------------------------------------------------- recupero i post ed i commenti e le risorse
                if(request.getAttribute("postsList") != null)
                {
                    ArrayList <Commento> commentList = null;
                    ArrayList <Sollecitazione> sollList = null;
                    Risorsa risorsa = null;
                    if(request.getAttribute("sollList") != null)
                        sollList = (ArrayList <Sollecitazione>) request.getAttribute("sollList");
                    ArrayList <Post> postList = (ArrayList <Post>) request.getAttribute("postsList");
                    if(request.getAttribute("commentsList") != null)
                        commentList = (ArrayList <Commento>) request.getAttribute("commentsList");
                    if(request.getAttribute("risorsa") != null)
                        risorsa = (Risorsa) request.getAttribute("risorsa");
                    if(postList!=null && postList.size()!=0){
                        out.print("\t\t<div id='posts'>\n");
                        out.print("\t\t\t<label class='ptit'>post</label>\n");
                        for(int i=0;i<postList.size();i++){
                            boolean isPubblico=false,isPrivato=false,isDocente=false,AuthorPostEqualsUsername = postList.get(i).getAttore().equals(session.getAttribute("username"));
                            if(postList.get(i).getVisibilita().equalsIgnoreCase("public")){
                                isPubblico=true;}
                            else if(postList.get(i).getVisibilita().equalsIgnoreCase("private")){
                                isPrivato=true;}
                            else if(postList.get(i).getVisibilita().equals("docente")){
                                isDocente=true;}
                            //--------------------------------------------------------------------- post
                            if(isPubblico || ((isDocente) && (AuthorPostEqualsUsername || attoreCorpoDocente)) || (isPrivato && AuthorPostEqualsUsername)){
                                if(postList.get(i).isIsBozza()==true){//se il post è una bozza
                                    if(AuthorPostEqualsUsername){//se l'autore della bozza è loggato al momento
                                        out.print("\t\t\t<div class='postDraft'>\n");
                                        out.print("\t\t\t\t<img class='deletePost' src='images/various/delete.png' title='elimina post' onClick='deletePost("+postList.get(i).getId()+");'/>\n");
                                        //---------------------------------------------------------------------form per modificare post
                                        out.print("\t\t\t\t<form name='formModificaPost' action='Controller' method='post'>\n");
                                        out.print("\t\t\t\t\t<input type='hidden' name='post_id' value='"+postList.get(i).getId()+"'/>\n");
                                        out.print("\t\t\t\t\t<input type='hidden' id='type' name='type' value='modificaPost'/>\n");
                                        out.print("\t\t\t\t\t<input class='modificaPost' title='modifica post' type='image' src='images/various/modifica.png'/>\n");
                                        out.print("\t\t\t\t</form>\n");
                                        //----------------------------------------------------------------------------------------------------
                                        out.print("\t\t\t\t<img class='saveDraft' src='images/various/save.png' title='salva bozza' onClick='saveDraft("+postList.get(i).getId()+");'/>\n");
                                        if(postList.get(i).getTitolo()!=null)
                                        out.print("\t\t\t\t<label class='postTitolo'>"+postList.get(i).getTitolo()+"</label>\n");
                                        out.print("\t\t\t\t<br/>"+postList.get(i).getAttore()+" ha scritto:<br/>\n");
                                        out.print("\t\t\t\t\""+postList.get(i).getTesto()+"\"<br/>\n");
                                        if(postList.get(i).getId_risorsa()!=null && postList.get(i).getId_risorsa()!=0){
                                            out.print("\t\t\t\t<label class='risorsa'>Risorsa:</label> <br/>\n");
                                            out.print("\t\t\t\t<a class='risorsalink' href='"+risorsa.getUrl()+"'>"+risorsa.getNome()+"</a><br/>\n");
                                        }   
                                        out.print("\t\t\t\t<label class='date'>"+postList.get(i).getData()+"&nbsp;</label> <br/>\n");
                                        out.print("\t\t\t\t<label class='time'>"+postList.get(i).getOra()+"&nbsp;</label>\n");
                                        out.print("\t\t\t</div>\n\n");
                                    }
                                }
                                else{//se il post non è una bozza
                                    out.print("\t\t\t<div class='post'>\n");
                                    if(AuthorPostEqualsUsername){
                                        out.print("\t\t\t\t<img class='deletePost' src='images/various/delete.png' title='elimina post' onClick='deletePost("+postList.get(i).getId()+");'/>\n");
                                        out.print("\t\t\t\t<img class='trasformaInBozzaImg' src='images/various/draft2.jpg' title='riporta in bozza' onClick='returnToDraft("+postList.get(i).getId()+");'/>\n");
                                    }
                                    if(postList.get(i).getTitolo()!=null)
                                        out.print("\t\t\t\t<label class='postTitolo'>"+postList.get(i).getTitolo()+"</label>\n");
                                    out.print("\t\t\t\t<br/>"+postList.get(i).getAttore()+" ha scritto:<br/>\n");
                                    out.print("\t\t\t\t\""+postList.get(i).getTesto()+"\"<br/>\n");
                                    if(postList.get(i).getId_risorsa()!=null && postList.get(i).getId_risorsa()!=0){
                                        out.print("\t\t\t\t<label class='risorsa'>Risorsa:</label> <br/>\n");
                                        out.print("\t\t\t\t<a class='risorsalink' href='"+risorsa.getUrl()+"'>"+risorsa.getNome()+"</a><br/>\n");
                                    }
                                    out.print("\t\t\t\t<label class='date'>"+postList.get(i).getData()+"&nbsp;</label> <br/>\n");
                                    out.print("\t\t\t\t<label class='time'>"+postList.get(i).getOra()+"&nbsp;</label>\n");
                                    out.print("\t\t\t</div>\n\n");
                                }
                                //---------------------------------------------------------------------- comment
                                if(commentList != null){
                                    boolean AuthorCommentEqualsUsername;
                                    for(int j=0;j<commentList.size();j++){
                                        if((commentList.get(j).getId_contributo()).equals(postList.get(i).getId())){
                                            AuthorCommentEqualsUsername = commentList.get(j).getAttore().equals(session.getAttribute("username"));
                                            out.print("\t\t\t<div class='comment'>\n");
                                            if(AuthorCommentEqualsUsername){
                                                out.print("\t\t\t\t<img class='deleteCommento' src='images/various/delete.png' title='elimina commento' onClick='eliminaCommento("+commentList.get(j).getId()+");'/>\n");
                                            }
                                            out.print("\t\t\t\t"+commentList.get(j).getAttore()+" ha risposto:<br/>\n");
                                            out.print("\t\t\t\t\""+commentList.get(j).getTesto()+"\"<br/>\n");
                                            out.print("\t\t\t</div>\n");
                                        }
                                    }
                                }
                                out.print("\n");
                                //---------------------------------------------------------------------- text for new comment
                                if(postList.get(i).isIsBozza()==false){
                                    out.print("\t\t\t<div class='addComment'>\n");
                                    out.print("\t\t\t\t<input class='inputComment' type='text' onkeypress='Javascript: if (event.keyCode==13) addNewComment(this.value,"+postList.get(i).getId()+");'><br/>\n");
                                    out.print("\t\t\t\t<a href='#' class='addFile' onClick='showFilePicker(fileID"+postList.get(i).getId()+")'>Aggiungi file</a>\n");
                                    out.print("\t\t\t\t<input id='fileID"+postList.get(i).getId()+"' class='inputComment' type='file' style='display: none;'>\n");
                                    out.print("\t\t\t</div>\n");
                                }
                            }
                        }
                    }
                    out.print("\t\t</div>\n");
                    if(sollList!=null && sollList.size()!=0){
                        out.print("\t\t<div id='sollecitazioni'>\n");
                        out.print("\t\t\t<label class='ptit'>sollecitazioni</label>\n");
                        for(int k=0;k<sollList.size();k++){
                            if(!sollList.get(k).isIsBozza()){//non è una bozza
                                out.print("\t\t\t<div class='sollecitazione'>\n");
                                if(sollList.get(k).getTitolo()!=null)
                                    out.print("\t\t\t\t<label class='sollTitolo'>"+sollList.get(k).getTitolo()+"</label>\n");
                                if(sollList.get(k).getAttore()!=null)
                                    out.print("\t\t\t\t<br/>"+sollList.get(k).getAttore()+" ha scritto:<br/>\n");
                                if(sollList.get(k).getTesto()!=null)
                                    out.print("\t\t\t\t\""+sollList.get(k).getTesto()+"\"<br/>\n");
                                if(sollList.get(k).getId_risorsa()!=null && sollList.get(k).getId_risorsa()!=0){
                                    out.print("\t\t\t\t<label class='risorsa'>Risorsa:</label> <br/>\n");
                                    out.print("\t\t\t\t<a class='risorsalink' href='"+risorsa.getUrl()+"'>"+risorsa.getNome()+"</a><br/>\n");
                                }
                                out.print("\t\t\t\tData di scadenza: <label class='sollDate'>"+sollList.get(k).getDataScadenza()+"</label><br/>\n");
                                //------------------------------------------------form per creaRisposta
                                out.print("\t\t\t\t<form action='Controller' method='post'>\n");//senza plugin
                                    out.print("\t\t\t\t\t<input type='hidden' name='soll_id' value='"+sollList.get(k).getId()+"'/>\n");
                                    out.print("\t\t\t\t\t<input type='hidden' name='plugin' value='no'/>\n");
                                    out.print("\t\t\t\t\t<input type='hidden' id='type' name='type' value='creaRisposta'/>\n");
                                    out.print("\t\t\t\t\t<input type='submit' value='Rispondi'/>\n");
                                out.print("\t\t\t\t</form>\n");
                                out.print("\t\t\t\t<form action='Controller' method='post'>\n");//con plugin
                                    out.print("\t\t\t\t\t<input type='hidden' name='soll_id' value='"+sollList.get(k).getId()+"'/>\n");
                                    out.print("\t\t\t\t\t<input type='hidden' name='plugin' value='yes'/>\n");
                                    out.print("\t\t\t\t\t<input type='hidden' id='type' name='type' value='creaRisposta'/>\n");
                                    out.print("\t\t\t\t\t<input type='submit' value='Rispondi con plugin'/>\n");
                                out.print("\t\t\t\t</form>\n");
                                out.print("\t\t\t\t<form action='Controller' method='post'>\n");//con plugin
                                    out.print("\t\t\t\t\t<input type='hidden' name='soll_id' value='"+sollList.get(k).getId()+"'/>\n");
                                    out.print("\t\t\t\t\t<input type='hidden' id='type' name='type' value='vediRisposte'/>\n");
                                    out.print("\t\t\t\t\t<input type='submit' value='Vedi risposte'/>\n");
                                out.print("\t\t\t\t</form>\n");
                                //------------------------------------------------fine sollecitazione
                                out.print("\t\t\t</div>\n");
                            }else{//è una bozza
                                if(attoreCorpoDocente){
                                    out.print("\t\t\t<div class='sollecitazioneBozza'>\n");
                                    //---------------------------------------------------------------------form per modificare sollecitazione
                                    out.print("\t\t\t\t<form action='Controller' method='post'>\n");
                                    out.print("\t\t\t\t\t<input type='hidden' name='soll_id' value='"+sollList.get(k).getId()+"'/>\n");
                                    out.print("\t\t\t\t\t<input type='hidden' id='type' name='type' value='modificaSoll'/>\n");
                                    out.print("\t\t\t\t\t<input class='modificaSollImg' title='modifica sollecitazione' type='image' src='images/various/modifica.png'/>\n");
                                    out.print("\t\t\t\t</form>\n");
                                    out.print("\t\t\t\t<img class='deleteSollecitazione' src='images/various/delete.png' title='elimina sollecitazione' onClick='eliminaSollecitazione("+sollList.get(k).getId()+");'/>\n");
                                    if(sollList.get(k).getTitolo()!=null)
                                                 out.print("\t\t\t\t<label class='sollTitolo'>"+sollList.get(k).getTitolo()+"</label>\n");
                                    out.print("\t\t\t\t<br/>"+sollList.get(k).getAttore()+" ha scritto:<br/>\n");
                                    out.print("\t\t\t\t\""+sollList.get(k).getTesto()+"\"<br/>\n");
                                    if(sollList.get(k).getId_risorsa()!=null && sollList.get(k).getId_risorsa()!=0){
                                            out.print("\t\t\t\t<label class='risorsa'>Risorsa:</label> <br/>\n");
                                            out.print("\t\t\t\t<a class='risorsalink' href='"+risorsa.getUrl()+"'>"+risorsa.getNome()+"</a><br/>\n");
                                    }
                                    out.print("\t\t\t\tData di scadenza: <label class='sollDate'>"+sollList.get(k).getDataScadenza()+"<label><br/>\n");
                                    out.print("\t\t\t</div>\n");
                                }
                            }
                        }
                        out.print("\t\t</div>\n");
                    }
                    
                }
                
                
            %>
        </article>
    </body>
</html>
