<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="classes.partedinamica.Sollecitazione" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" media="screen" href="style/style.css"/>
        <title>Sollecitazione page</title>
    </head>
    <body>
        <%@ include file="./common/header.txt"%>
        <%@ include file="./common/nav.txt"%>
        <%@ include file="./common/userInfo.txt"%>
        <article>
            <%
                if(session.getAttribute("username")== null || ((String)session.getAttribute("username")).equals("guest")){
                    RequestDispatcher jspErrore = getServletContext().getRequestDispatcher("/error.jsp");
                    request.setAttribute("errorDescription", "Impossibile effettuare la richiesta. Si prega di accedere al sistema prima");
                    jspErrore.forward(request,response);
                }
            %>
            <div id="creaNuovaSollecitazione">
                <form action='Controller' method='post'>
                    <%
                        Sollecitazione sol = null;
                        if (request.getAttribute("soll") != null) {//per la modifica
                            sol = (Sollecitazione) request.getAttribute("soll");
                        }
                        if (request.getAttribute("sollecit") != null) {//per la creazione ex-novo
                            sol = (Sollecitazione) request.getAttribute("sollecit");
                        }
                        out.print("\tTitolo: </br>\n");
                        if (sol.getTitolo() != null) {
                            out.print("\t\t\t<input type='text' class='inputText' maxlength='30' name='textTitle' value='" + sol.getTitolo() + "'/><br/>\n");
                        } else {
                            out.print("\t\t\t<input type='text' class='inputText' maxlength='30' name='textTitle'/><br/>\n");
                        }
                        out.print("\t\t\tTesto: </br>\n");
                        if (sol.getTesto() != null) {
                            out.print("\t\t\t<input type='text' class='inputText' name='text' value='" + sol.getTesto() + "'/><br/>\n");
                        } else {
                            out.print("\t\t\t<input type='text' class='inputText' name='text'/><br/>\n");
                        }
                        out.print("\t\t\tUDA:&nbsp;</br>\n");
                        if (sol.getId_uda() != null) {
                            out.print("\t\t\t<input type='text' class='inputText' name='uda' value='1394' readonly/><br/>\n");
                        } else {
                            out.print("\t\t\t<input type='text' class='inputText' name='uda' value='1394' readonly/><br/>\n");
                        }
                        out.print("\t\t\tAllegati: &nbsp;</br>\n");
                        if (sol.getId_risorsa() == null) {
                            out.print("\t\t\t<input type='checkbox' name='risorsaAllegata'>Abilita il salvataggio della risorsa<br/>\n");
                        } else {
                            out.print("\t\t\t<input type='checkbox' name='risorsaAllegata' checked>Abilita il salvataggio della risorsa<br/>\n");
                        }
                        out.print("\t\t\t<input type='text' class='inputText' name='alleg' value='23' readonly/><br/>\n");
                        out.print("\t\t\tVisibilità:&nbsp;</br>\n");
                        if (sol.getVisibilita() != null) {
                            out.print("\t\t\t<select class='inputText' name='visibility' >\n");
                            if (sol.getVisibilita().equals("public")) {
                                out.print("\t\t\t\t<option value='public' selected>Visibile a tutti i partecipanti</option>\n");
                            } else {
                                out.print("\t\t\t\t<option value='public'>Visibile a tutti i partecipanti</option>\n");
                            }
                            if (sol.getVisibilita().equals("docente")) {
                                out.print("\t\t\t\t<option value='docente' selected>Visibile solo al corpo docente</option>\n");
                            } else {
                                out.print("\t\t\t\t<option value='docente'>Visibile solo al corpo docente</option>\n");
                            }
                            if (sol.getVisibilita().equals("private")) {
                                out.print("\t\t\t\t<option value='private' selected>Solo autore </option>\n");
                            } else {
                                out.print("\t\t\t\t<option value='private'>Solo autore </option>\n");
                            }
                            out.print("\t\t\t</select><br/>\n");
                        } else {
                            out.print("\t\t\t<select class='inputText' name='visibility' >\n");
                            out.print("\t\t\t\t<option value='public'>Visibile a tutti i partecipanti</option>\n");
                            out.print("\t\t\t\t<option value='docente'>Visibile solo al corpo docente</option>\n");
                            out.print("\t\t\t\t<option value='private'>Solo autore </option>\n");
                            out.print("\t\t\t</select><br/>\n");
                        }
                        //--------------------------------------------------------------------------------------------------------------------
                        out.print("\t\t\tVisibilità delle risposte:&nbsp;</br>\n");
                        if (sol.getVisibilitaRisposte() != null) {
                            out.print("\t\t\t<select class='inputText' name='visibilitaRisposte' >\n");
                            if (sol.getVisibilitaRisposte().equals("public")) {
                                out.print("\t\t\t\t<option value='public' selected>Visibile a tutti i partecipanti</option>\n");
                            } else {
                                out.print("\t\t\t\t<option value='public'>Visibile a tutti i partecipanti</option>\n");
                            }
                            if (sol.getVisibilitaRisposte().equals("docente")) {
                                out.print("\t\t\t\t<option value='docente' selected>Visibile solo al corpo docente</option>\n");
                            } else {
                                out.print("\t\t\t\t<option value='docente'>Visibile solo al corpo docente</option>\n");
                            }
                            out.print("\t\t\t</select><br/>\n");
                        } else {
                            out.print("\t\t\t<select class='inputText' name='visibilitaRisposte' >\n");
                            out.print("\t\t\t\t<option value='public'>Visibile a tutti i partecipanti</option>\n");
                            out.print("\t\t\t\t<option value='docente'>Visibile solo al corpo docente</option>\n");
                            out.print("\t\t\t</select><br/>\n");
                        }
                        //--------------------------------------------------------------------------------------------------------------------
                        if (sol.getDataScadenza() != null)//-------------------------------------------------imposto data scadenza
                        {
                            out.print("\t\t\t<input type='date' name='dataScadenza' value='" + sol.getDataScadenza() + "'/><br/>\n");
                        } else {
                            out.print("\t\t\t<input type='date' name='dataScadenza'/><br/>\n");
                        }
                        out.print("\t\t\tVoto massimo:&nbsp;</br>\n");
                        if (sol.getVotoMax() != null)//-------------------------------------------------imposto data scadenza
                        {
                            out.print("\t\t\t<input type='text' name='votoMax' value='" + sol.getVotoMax()+ "'/><br/>\n");
                        }else {
                            out.print("\t\t\t<input type='text' name='votoMax'/><br/>\n");
                        }
                        if (sol.isIsBozza() == false) {
                            out.print("\t\t\t<input type='checkbox' name='isDraft'>Crea come bozza</input><br/>\n");
                        } else {
                            out.print("\t\t\t<input type='checkbox' name='isDraft' checked>Crea come bozza</input><br/>\n");
                        }
                        if (request.getAttribute("course_name") != null) {
                            out.print("\t\t\t<input type='hidden' id='type' name='type' value='aggiungiSollecitazione'/>\n");
                            out.print("\t\t\t<input type='hidden' name='course_name' value='" + request.getAttribute("course_name") + "'/>\n");
                        } else {
                            out.print("\t\t\t<input type='hidden' id='type' name='type' value='salvaSollecitazioneModificata'/>\n");
                            out.print("\t\t\t<input type='hidden' id='type' name='soll_id' value='" + sol.getId() + "'/>\n");
                            out.print("\t\t\t<input type='hidden' name='course_name' value='" + sol.getNome_corso() + "'/>\n");
                        }
                        if (request.getAttribute("course_id") != null) {
                            out.print("\t\t\t<input type='hidden' name='course_id' value='" + request.getAttribute("course_id") + "'/>\n");
                        } else {
                            out.print("\t\t\t<input type='hidden' name='course_id' value='" + sol.getId_corso() + "'/>\n");
                        }
                        out.print("\t\t\t<input type='hidden' name='plugin' value='no'/>\n");
                        out.print("\t\t\t<input type='submit' value='Ok'/>\n");
                        out.print("\t\t\t<input type='button' value='Esc' onClick='window.history.go(-1);'/>\n");
                    %>
                </form>
            </div>
        </article>
        <%@ include file="./common/footer.txt"%>
    </body>
</html>