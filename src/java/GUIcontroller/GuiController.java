package GUIcontroller;

import com.google.gson.Gson;
import controller.CommentoCtrl;
import controller.CorsoCtrl;
import controller.PostCtrl;
import controller.UtenteCtrl;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import classes.Corso;
import classes.Risorsa;
import classes.Utente;
import classes.Valutazione;
import classes.partedinamica.Commento;
import classes.partedinamica.Post;
import classes.partedinamica.Risposta;
import classes.partedinamica.Sollecitazione;
import controller.RisorsaCtrl;
import controller.RispostaCtrl;
import controller.SollecitazioneCtrl;
import controller.ValutazioneCtrl;
import java.sql.SQLException;

@WebServlet(urlPatterns = {"/Controller"})
public class GuiController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private HttpSession session;
    public static boolean DEBUG = false;

    /**
     * Starting method of a new instance of a servlet.It also create istance of
     * the java class for the databases and the RequestDispatcher towards the
     * error page
     *
     * @param config
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(GuiController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Meotodo per analizzare il tipo di richiesta in arrivo e reindirizzare
     * verso la funzione corretta
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        session = request.getSession();//recupero la sessione o la creo nel caso manchi
        if (session.getAttribute("username") == null) {
            session.setAttribute("username", "guest");//utenti non loggati diventato guest
            session.setAttribute("docente", "off");
            session.setAttribute("studente", "off");
            session.setAttribute("redattore", "off");
            session.setAttribute("esaminatore", "off");
            session.setAttribute("moderatore", "off");
        }
        if (request.getParameter("type") == null) {
            goToErrorPage(request, response, "Richiesta non riconosciuta. Si prega di riprovare");
        }
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:130 >> Tipo della richiesta: " + request.getParameter("type"));
        }

        //richiesta di identificare l'utente attraverso login
        if (request.getParameter("type").equals("login")) {
            doLogin(request, response);
        }
        //richiesta di sloggare l'utente attraverso logout
        if (request.getParameter("type").equals("logout")) {
            doLogout(request, response);
        }
        //richiesta di registrare nuovo utente attraverso register
        if (request.getParameter("type").equals("register")) {
            doRegister(request, response);
        }
        //richiesta di mostare le info dell'utente attraverso personalInfo
        if (request.getParameter("type").equals("personalInfo")) {
            sendPersonalInfo(request, response);
        }
        //richiesta di mostare tutti i corsi presenti attraverso listAllCourses
        if (request.getParameter("type").equals("listAllCourses")) {
            retrieveListAllCourses(request, response);
        }
        //richiesta di mostare tutti i post dato il nome del corso attraverso listPostByNameCourse
        if (request.getParameter("type").equals("listPostByNameCourse")) {
            listPostByNameCourse(request, response);
        }
        //richiesta di mostare info dettagliate del corso dato il nome del corso (CourseName) attraverso showCourse
        if (request.getParameter("type").equals("showCourse")) {
            showInfoCourse(request, response);
        }
        //richiesta di mostare tutti i post dato il nome dell'utente attraverso listPostByUserName
        if (request.getParameter("type").equals("listPostByUserName")) {
            listPostByUserName(request, response);
        }
        //richiesta di mostare tutti i corsi dell'anno specificato attraverso listCoursesByYear
        if (request.getParameter("type").equals("listCoursesByYear")) {
            retrieveListCoursesSelectedYear(request, response);
        }
        //richiesta di aggiungere un nuovo commento attraverso newComment
        if (request.getParameter("type").equals("newComment")) {
            addNewComment(request, response);
        }
        //richiesta di aggiungere un nuovo post per il redirect attraverso newPost
        if (request.getParameter("type").equals("newPost")) {
            creaPost(request, response);
        }
        //richiesta di eliminare un post attraverso deletePost
        if (request.getParameter("type").equals("deletePost")) {
            deletePost(request, response);
        }
        //richiesta di salvare una bozza come definitivo saveDraft
        if (request.getParameter("type").equals("saveDraft")) {
            transformaDraftInPost(request, response);
        }
        //richiesta di cambiare i permessi attraverso setPermission
        if (request.getParameter("type").equals("setPermission")) {
            setPermessi(request, response);
        }
        //richiesta di aggiungere un nuovo post attraverso aggiungiPost
        if (request.getParameter("type").equals("aggiungiPost")) {
            aggiungiPost(request, response);
        }
        //richiesta di modificare un post attraverso modificaPost
        if (request.getParameter("type").equals("modificaPost")) {
            modificaPost(request, response);
        }
        //richiesta di modificare un post attraverso salvaPostModificato
        if (request.getParameter("type").equals("salvaPostModificato")) {
            salvaPostModificato(request, response);
        }
        //richiesta di creare una sollecitazione per redirect attraverso newSollecitazione
        if (request.getParameter("type").equals("newSollecitazione")) {
            creaSollecitazione(request, response);
        }
        //richiesta di aggiungere un nuova sollecitazione attraverso aggiungiSollecitazione
        if (request.getParameter("type").equals("aggiungiSollecitazione")) {
            aggiungiSollecitazione(request, response);
        }
        //richiesta di recuperare tutte sollecitazione attraverso listAllSollecitazioni
        if (request.getParameter("type").equals("listAllSollecitazioni")) {
            recuperaTutteSollecitazioni(request, response);
        }
        //richiesta di trasformare in bozza un post attraverso returnToDraft
        if (request.getParameter("type").equals("returnToDraft")) {
            trasformaInBozza(request, response);
        }
        //richiesta di modificare una sollecitazione attraverso modificaSoll
        if (request.getParameter("type").equals("modificaSoll")) {
            modificaSollecitazione(request, response);
        }
        //richiesta di modificare una sollecitazione attraverso salvaSollecitazioneModificato
        if (request.getParameter("type").equals("salvaSollecitazioneModificata")) {
            salvaSollecitazioneModificata(request, response);
        }
        //richiesta di creare una risposta per redirect attraverso creaRisposta
        if (request.getParameter("type").equals("creaRisposta")) {
            creaRisposta(request, response);
        }
        //richiesta di aggiungere un nuova risposta attraverso aggiungiRisposta
        if (request.getParameter("type").equals("aggiungiRisposta")) {
            aggiungiRisposta(request, response);
        }
        //richiesta di elencare tutte le risposta di una data sollecitazione attraverso vediRisposte
        if (request.getParameter("type").equals("vediRisposte")) {
            vediRisposte(request, response);
        }
        //richiesta di salvare una valutazione attraverso salvaValutazione
        if (request.getParameter("type").equals("salvaValutazione")) {
            salvaValutazione(request, response);
        }
        //richiesta di elencare tutte valutazioni attraverso listValutazioni
        if (request.getParameter("type").equals("listValutazioni")) {
            recuperaTutteValutazioni(request, response);
        }
        //richiesta di eliminare il commento attraverso deleteCommento
        if (request.getParameter("type").equals("deleteCommento")) {
            eliminaCommento(request, response);
        }
        //richiesta di eliminare il commento attraverso deleteSollecitazione
        if (request.getParameter("type").equals("deleteSollecitazione")) {
            eliminaSollecitazione(request, response);
        }
    }

    /**
     * Recupera la data di sistema
     *
     */
    public String getDate() {
        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateNow = formatter.format(currentDate.getTime());
        return dateNow;
    }

    /**
     * Recupera l'ora del sistema
     *
     */
    public String getTime() {
        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        String dateNow = formatter.format(currentDate.getTime());
        return dateNow;
    }

    /**
     * Controlla i permessi dell'utente a seconda dei permessi richiesti
     *
     * @param request
     * @param response
     * @param perm indicante il tipo di permesso richiesto
     * @throws ServletException
     * @throws IOException
     */
    public void checkPermission(HttpServletRequest request, HttpServletResponse response, String perm)
            throws ServletException, IOException {
        if (perm.equals("needToBeLogged")) {//l'utente deve essere loggato (= sessione già creata e non essere guest)
            if (session.getAttribute("username") == null || ((String) session.getAttribute("username")).equals("guest")) {
                goToErrorPage(request, response, "Utente non riconosciuto. Si prega di effettuare l'accesso al sistema");
            }
        }
    }

    /**
     * Ridireziona l'utente verso la pagina di errore con la descrizione
     * dell'errore richiesta
     *
     * @param request
     * @param response
     * @param message messaggio di errore che la pagina di errore mostrera
     * @throws ServletException
     * @throws IOException
     */
    public void goToErrorPage(HttpServletRequest request, HttpServletResponse response, String mess)
            throws ServletException, IOException {
        RequestDispatcher jspErrore = getServletContext().getRequestDispatcher("/error.jsp");
        request.setAttribute("errorDescription", mess);
        jspErrore.forward(request, response);
    }

    /**
     * Richiesta di loggarsi al sistema
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void doLogin(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/personalPage.jsp");
        if (request.getParameter("username") == null || request.getParameter("password") == null) {
            goToErrorPage(request, response, "Username e/o Password non inserite");
        }
        if (UtenteCtrl.checkLogin(request.getParameter("username"), request.getParameter("password"))) {
            session.setAttribute("username", request.getParameter("username"));
            rd0.forward(request, response);
        } else {
            goToErrorPage(request, response, "Username e/o Password non valide");
        }
    }

    /**
     * Richiesta di sloggarsi dal sistema
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void doLogout(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        checkPermission(request, response, "needToBeLogged");
        RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/index.jsp");
        session.removeAttribute("username");
        session.invalidate();
        rd0.forward(request, response);
    }

    /**
     * Richiesta di registrazione di un nuovo utente presso il sistema
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void doRegister(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/index.jsp");
        if (request.getParameter("username") == null || request.getParameter("password") == null || request.getParameter("email") == null || request.getParameter("username").equalsIgnoreCase("guest")) {
            goToErrorPage(request, response, "Username e/o Password e/o email non inserite");
        } else {
            if (UtenteCtrl.addUser(request.getParameter("username"), request.getParameter("password"), request.getParameter("email"))) {
                rd0.forward(request, response);
            } else {
                goToErrorPage(request, response, "Username e/o Password e/o email non inserite");
            }
        }
    }

    /**
     * Richiesta di recupero di tutte le informazioni riguardanti l'utente
     * indicato
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void sendPersonalInfo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        checkPermission(request, response, "needToBeLogged");
        Utente user = UtenteCtrl.retrievePersonalInfo((String) session.getAttribute("username"));
        if (user == null) {
            goToErrorPage(request, response, "Utente non riconosciuto");
        }
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();
        String jsonUser = gson.toJson(user);
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:365>> sendPersonalInfo!AjaxAnswer: " + jsonUser);
        }
        out.print(jsonUser);
        out.flush();
    }

    /**
     * Richiesta di recuperare la lista di tutti i corsi
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void retrieveListAllCourses(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, ClassNotFoundException, IOException, InstantiationException, IllegalAccessException {
        checkPermission(request, response, "needToBeLogged");
        List<Corso> list = CorsoCtrl.retrieveListAllCourses();
        if (list == null) {
            goToErrorPage(request, response, "Impossibile recuperare le informazioni. Si prega di riprovare");
        }
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();
        String jsonUser = gson.toJson(list);
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:394>> sendPersonalInfo!AjaxAnswer: " + jsonUser);
        }
        out.print(jsonUser);
        out.flush();
    }

    /**
     * Richiesta di recuperare la lista di tutti i corsi con anno specifico
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void retrieveListCoursesSelectedYear(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, ClassNotFoundException, IOException, InstantiationException, IllegalAccessException {
        checkPermission(request, response, "needToBeLogged");
        List<Corso> list = CorsoCtrl.retrieveListCoursesByYear(request.getParameter("year"));
        if (list == null) {
            goToErrorPage(request, response, "Impossibile recuperare le informazioni. Si prega di riprovare");
        }
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();
        String jsonUser = gson.toJson(list);
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:423>> sendPersonalInfo!AjaxAnswer: " + jsonUser);
        }
        out.print(jsonUser);
        out.flush();
    }

    /**
     * Richiesta di recuperare tutti i post di un dato corso
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void listPostByNameCourse(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        List<Post> list = PostCtrl.recuperaPostDatoNomeCorso(request.getParameter("CourseName"));
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:445>> PrintoPost: " + list);
        }
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();
        String jsonPost = gson.toJson(list);
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:452>> listPostByNameCourse!AjaxAnswer: " + jsonPost);
        }
        out.print(jsonPost);
        out.flush();
    }

    /**
     * Richiesta di recuperare tutti i post di un dato utente
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void listPostByUserName(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        List<Post> list = PostCtrl.recuperaPostDatoNomeUtente((String) session.getAttribute("username"));
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:474>> PrintoPost: " + list);
        }
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();
        String jsonPost = gson.toJson(list);
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:481>> listPostByUserName!AjaxAnswer: " + jsonPost);
        }
        out.print(jsonPost);
        out.flush();
    }

    /**
     * Richiesta di recuperare tutte le informazioni del corso comprensivo di
     * post,commenti e sollecitazioni
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void showInfoCourse(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, ClassNotFoundException, IOException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        RequestDispatcher jCourse = getServletContext().getRequestDispatcher("/course.jsp");
        Corso course = CorsoCtrl.retrieveCourseInfo(request.getParameter("CourseName"));
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:504>> PrintoCorso: " + course);
        }
        if (course == null) {
            goToErrorPage(request, response, "Impossibile recuperare le informazioni. Si prega di riprovare");
        }
        request.setAttribute("CourseName", course.getName());
        request.setAttribute("CourseDescription", course.getDescription());
        request.setAttribute("CourseID", course.getId());
        //----------------------------------------------------------------------- recupero la lista dei post
        List<Post> Listposts = PostCtrl.recuperaPostDatoNomeCorso(request.getParameter("CourseName"));
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:515>> PrintoPost: " + Listposts);
        }
        request.setAttribute("postsList", Listposts);
        //----------------------------------------------------------------------- recupero la lista delle sollecitazione
        List<Sollecitazione> listSoll = SollecitazioneCtrl.recuperaTutteSollecitazione(course.getId());
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:521>> PrintoSollecitazioni: " + listSoll);
        }
        request.setAttribute("sollList", listSoll);
        //----------------------------------------------------------------------- recupero la lista delle risorse
        Risorsa ris = RisorsaCtrl.load("23");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:527>> PrintoRisorsa: " + ris);
        }
        request.setAttribute("risorsa", ris);
        //----------------------------------------------------------------------- prendo la lista dei commenti
        List<Commento> ListComments = CommentoCtrl.retrieveAllComments();
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:533>> PrintoComments: " + ListComments);
        }
        request.setAttribute("commentsList", ListComments);
        jCourse.forward(request, response);
    }

    /**
     * Richiesta di creazione e salvataggio di un commento
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void addNewComment(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        Object attore = session.getAttribute("username");
        Object testo = request.getParameter("text");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:556<< risorsa: " + request.getParameter("risorsa"));
        }
        Object risorsa = request.getParameter("risorsa");
        Object post = request.getParameter("post_id");
        CommentoCtrl.addComment(attore, testo, risorsa, post);
        PrintWriter out = response.getWriter();
        out.print("");
        out.flush();
    }

    /**
     * Richiesta di creazione di un post per ridirezionare verso post.jsp
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void creaPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        if (request.getParameter("plugin").equals("no")) {
            Object id_corso = request.getParameter("course_id");
            Object nome_corso = request.getParameter("course_name");
            Post pst = PostCtrl.creaPost(null, null, id_corso, null, null, null, nome_corso, null, null, null, null, null);
            RequestDispatcher jcl = getServletContext().getRequestDispatcher("/post.jsp");
            request.setAttribute("post", pst);
            request.setAttribute("course_id", id_corso);
            request.setAttribute("course_name", nome_corso);
            jcl.forward(request, response);
        } else {
            System.out.println(">>GUICONTROLLER:590>> Richiesta di plugin per creaPost");
            RequestDispatcher jc = getServletContext().getRequestDispatcher("/personalPage.jsp");
            jc.forward(request, response);
        }
    }

    /**
     * Richiesta di creazione di una nuova sollecitazione da reindirizzare verso
     * sollecitazione.jsp
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void creaSollecitazione(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        RequestDispatcher jsl = getServletContext().getRequestDispatcher("/sollecitazione.jsp");
        if (request.getParameter("plugin").equals("no")) {
            Object id_corso = request.getParameter("course_id");
            Object nome_corso = request.getParameter("course_name");
            Sollecitazione sll = SollecitazioneCtrl.creaSollecitazione(id_corso, nome_corso);
            if (DEBUG) {
                System.out.println(">>GUICONTROLLER:617<< sollecitazione: " + sll);
            }
            request.setAttribute("sollecit", sll);
            request.setAttribute("course_id", id_corso);
            request.setAttribute("course_name", nome_corso);
            jsl.forward(request, response);
        } else {
            System.out.println(">>GUICONTROLLER:624>> Richiesta di plugin per creaPost");
            RequestDispatcher jc = getServletContext().getRequestDispatcher("/personalPage.jsp");
            jc.forward(request, response);
        }
    }

    /**
     * Richiesta di creazione di una nuova risposta da reindirizzare verso
     * risposta.jsp
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void creaRisposta(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        RequestDispatcher jsl = getServletContext().getRequestDispatcher("/risposta.jsp");
        if (request.getParameter("plugin").equals("no")) {
            Object id_sollecitazione = request.getParameter("soll_id");
            Object attore = session.getAttribute("username");
            Risposta ris = RispostaCtrl.creaRisposta(id_sollecitazione, attore);
            if (DEBUG) {
                System.out.println(">>GUICONTROLLER:668<< risposta: " + ris);
            }
            request.setAttribute("rispostaNuova", ris);
            jsl.forward(request, response);
        } else {
            System.out.println(">>GUICONTROLLER:624>> Richiesta di plugin per creaPost");
            RequestDispatcher jc = getServletContext().getRequestDispatcher("/personalPage.jsp");
            jc.forward(request, response);
        }
    }

    /**
     * Richiesta di modificare un post per reindirizzare a post.jsp
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void modificaPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:645<< modificaPost: Chiamato metodo con id" + request.getParameter("post_id"));
        }
        Object post_id = request.getParameter("post_id");
        Post pst = PostCtrl.caricaPost(post_id);
        request.setAttribute("post", pst);
        RequestDispatcher jcl = getServletContext().getRequestDispatcher("/post.jsp");
        jcl.forward(request, response);
    }

    /**
     * Richiesta di modificare una sollecitazione per reindirizzare a
     * sollecitazione.jsp
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void modificaSollecitazione(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:678<< modificaSollecitazione: Chiamato metodo con id" + request.getParameter("soll_id"));
        }
        Object soll_id = request.getParameter("soll_id");
        Sollecitazione soll = SollecitazioneCtrl.caricaSollecitazione(soll_id);
        request.setAttribute("soll", soll);
        RequestDispatcher jcl = getServletContext().getRequestDispatcher("/sollecitazione.jsp");
        jcl.forward(request, response);
    }

    /**
     * Richiesta di creazione e salvataggio di un post
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void aggiungiPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        Object id_corso = request.getParameter("course_id");
        Object nome_corso = request.getParameter("course_name");
        Object attore = session.getAttribute("username");
        Object testo = request.getParameter("textPost");
        Object data = getDate();
        Object ora = getTime();
        Object id_risorsa;
        if (request.getParameter("risorsaAllegata") == null) {
            id_risorsa = null;
        } else {
            id_risorsa = request.getParameter("alleg");
        }
        Object id_uda = request.getParameter("uda");
        Object isBozza = request.getParameter("isDraft");
        Object plugin = null;
        Object titolo = request.getParameter("textTitle");
        Object visibilita = request.getParameter("visibility");
        PostCtrl.aggiungiPost(attore, data, id_corso, id_risorsa, id_uda, isBozza, nome_corso, ora, plugin, titolo, visibilita, testo);
        RequestDispatcher jc = getServletContext().getRequestDispatcher("/personalPage.jsp");
        jc.forward(request, response);
    }

    /**
     * Richiesta di creazione e salvataggio di una sollecitazione
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void aggiungiSollecitazione(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        Object id_corso = request.getParameter("course_id");
        Object nome_corso = request.getParameter("course_name");
        Object attore = session.getAttribute("username");
        Object testo = request.getParameter("text");
        Object visibilitaRisposte = request.getParameter("visibilitaRisposte");
        Object votoMax = request.getParameter("votoMax");
        Object data = getDate();
        Object ora = getTime();
        Object id_risorsa;
        if (request.getParameter("risorsaAllegata") == null) {
            id_risorsa = null;
        } else {
            id_risorsa = request.getParameter("alleg");
        }
        Object id_uda = request.getParameter("uda");
        Object isBozza = request.getParameter("isDraft");
        Object titolo = request.getParameter("textTitle");
        Object visibilita = request.getParameter("visibility");
        Object dataScadenza = request.getParameter("dataScadenza");
        SollecitazioneCtrl.aggiungiSollecitazione(id_risorsa, attore, testo, titolo, data, ora, visibilita, nome_corso, isBozza, id_corso, id_uda, dataScadenza,votoMax,visibilitaRisposte);
        RequestDispatcher jc = getServletContext().getRequestDispatcher("/personalPage.jsp");
        jc.forward(request, response);
    }

    /**
     * Richiesta di creazione e salvataggio di una risposta
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void aggiungiRisposta(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        Object attore = request.getParameter("autore");
        Object testo = request.getParameter("text");
        Object id_contributo = request.getParameter("soll_id");
        Object plugin = null;
        Object id_valutazione = null;
        Object id_artefatto = null;
        Object id_risorsa;
        if (request.getParameter("risorsaAllegata") == null) {
            id_risorsa = null;
        } else {
            id_risorsa = request.getParameter("alleg");
        }
        RispostaCtrl.aggiungiRisposta(attore, testo, id_contributo, plugin, id_valutazione, id_artefatto, id_risorsa);
        RequestDispatcher jc = getServletContext().getRequestDispatcher("/personalPage.jsp");
        jc.forward(request, response);
    }

    /**
     * Richiesta di salvare un post modificato
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void salvaPostModificato(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:741<< id: " + request.getParameter("post_id"));
        }
        Object post_id = request.getParameter("post_id");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:745<< Object id: " + post_id);
        }
        Object id_corso = request.getParameter("course_id");
        Object nome_corso = request.getParameter("course_name");
        Object attore = session.getAttribute("username");
        Object testo = request.getParameter("textPost");
        Object data = getDate();
        Object ora = getTime();
        Object id_risorsa;
        if (request.getParameter("risorsaAllegata") == null) {
            id_risorsa = null;
        } else {
            id_risorsa = request.getParameter("alleg");
        }
        Object id_uda = request.getParameter("uda");
        Object isBozza = request.getParameter("isDraft");
        Object plugin = null;
        Object titolo = request.getParameter("textTitle");
        Object visibilita = request.getParameter("visibility");
        PostCtrl.salvaPostModificato(post_id, attore, data, id_corso, id_risorsa, id_uda, isBozza, nome_corso, ora, plugin, titolo, visibilita, testo);
        RequestDispatcher jc = getServletContext().getRequestDispatcher("/personalPage.jsp");
        jc.forward(request, response);
    }

    /**
     * Richiesta di salvare una sollecitazione modificata
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void salvaSollecitazioneModificata(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:823<< id: " + request.getParameter("soll_id"));
        }
        Object plugin = null;
        Object id = request.getParameter("soll_id");
        Object id_corso = request.getParameter("course_id");
        Object nome_corso = request.getParameter("course_name");
        Object attore = session.getAttribute("username");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:830<< test: " + request.getParameter("text"));
        }
        Object testo = request.getParameter("text");
        Object data = getDate();
        Object ora = getTime();
        Object id_risorsa;
        if (request.getParameter("risorsaAllegata") == null) {
            id_risorsa = null;
        } else {
            id_risorsa = request.getParameter("alleg");
        }
        Object id_uda = request.getParameter("uda");
        Object isBozza = request.getParameter("isDraft");
        Object titolo = request.getParameter("textTitle");
        Object visibilita = request.getParameter("visibility");
        Object dataScadenza = request.getParameter("dataScadenza");
        SollecitazioneCtrl.salvaSollecitazioneModificata(id, attore, data, id_corso, id_risorsa, id_uda, isBozza, nome_corso, ora, plugin, titolo, visibilita, testo, dataScadenza);
        RequestDispatcher jc = getServletContext().getRequestDispatcher("/personalPage.jsp");
        jc.forward(request, response);
    }

    /**
     * Richiesta di eliminazione di un post
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void deletePost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:784>> deletePost: id: " + request.getParameter("post_id"));
        }
        CommentoCtrl.deleteCommentsByPostID(request.getParameter("post_id"));
        PostCtrl.eliminaPost(request.getParameter("post_id"));
        PrintWriter out = response.getWriter();
        out.print("");
        out.flush();
    }

    /**
     * Richiesta di trasformare una bozza in un post definitivo
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public void transformaDraftInPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:808>> savePostDraft: id: " + request.getParameter("post_id"));
        }
        PostCtrl.transformaBozzaInPost(request.getParameter("post_id"));
        PrintWriter out = response.getWriter();
        out.print("");
        out.flush();
    }

    /**
     * Richiesta di cambio dei permessi dell'utente
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private void setPermessi(HttpServletRequest request, HttpServletResponse response) throws IOException {
        session.setAttribute(request.getParameter("actor"), request.getParameter("value"));
        PrintWriter out = response.getWriter();
        out.print("");
        out.flush();
    }

    /**
     * Richiesta di recuperare tutte le sollecitazioni
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws recuperaTutteSollecitazioni
     */
    private void recuperaTutteSollecitazioni(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        checkPermission(request, response, "needToBeLogged");
        List<Sollecitazione> listSoll = SollecitazioneCtrl.recuperaTutteSollecitazione(null);
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:846>> PrintoSollecitazioni: " + listSoll);
        }
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();
        String jsonSoll = gson.toJson(listSoll);
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:853>> listPostByNameCourse!AjaxAnswer: " + jsonSoll);
        }
        out.print(jsonSoll);
        out.flush();
    }

    /**
     * Richiesta di trasformare il post in bozza
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    private void trasformaInBozza(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:873>> deletePost: id: " + request.getParameter("post_id"));
        }
        CommentoCtrl.deleteCommentsByPostID(request.getParameter("post_id"));
        PostCtrl.transformaInBozza(request.getParameter("post_id"));
        PrintWriter out = response.getWriter();
        out.print("");
        out.flush();
    }

    /**
     * Richiesta di elencare tutte le risposta di una data sollecitazione
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    private void vediRisposte(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        RequestDispatcher jc = getServletContext().getRequestDispatcher("/risposte.jsp");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:873>> deletePost: id: " + request.getParameter("soll_id"));
        }
        List<Risposta> listRisp = RispostaCtrl.recuperaRisposteDataSollecitazione(request.getParameter("soll_id"));
        request.setAttribute("listRisp", listRisp);
        //----------------------------------------------------------------------- recupero la lista delle risorse
        Risorsa ris = RisorsaCtrl.load("23");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:527>> PrintoRisorsa: " + ris);
        }
        Sollecitazione sl = new Sollecitazione();
        sl.setId(new Integer(request.getParameter("soll_id")));
        sl.load();
        request.setAttribute("id_corso",sl.getId_corso());
        request.setAttribute("nome_corso",sl.getNome_corso());
        request.setAttribute("visRis",sl.getVisibilitaRisposte());
        request.setAttribute("votoMax",sl.getVotoMax());
        request.setAttribute("risorsa", ris);
        jc.forward(request, response);
    }

    /**
     * Richiesta di salvare una valutazione
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    private void salvaValutazione(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:823<<salvaValutazione idRisp: " + request.getParameter("id_risposta"));
        }
        Object id_risposta = request.getParameter("id_risposta");
        Object voto = request.getParameter("voto");
        Object nota = request.getParameter("nota");
        Object esaminatore = session.getAttribute("username");
        Object autore_risposta = request.getParameter("autore");
        Object id_corso = request.getParameter("id_corso");
        Object nome_corso = request.getParameter("nome_corso");
        ValutazioneCtrl.salvaValutazione(id_risposta,voto,nota,esaminatore,autore_risposta,id_corso,nome_corso);
        Risposta ris = new Risposta();
        ris.setId(new Integer((String)id_risposta));
        ris.load();
        ris.setId_valutazione(dbBroker.ValutazioneDB.getMaximumId());
        ris.salvaMod();
        PrintWriter out = response.getWriter();
        out.print("");
        out.flush();
    }

    /**
     * Richiesta di recuperare tutte le valutazioni
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws recuperaTutteSollecitazioni
     */
    private void recuperaTutteValutazioni(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        List <Valutazione> listVall = ValutazioneCtrl.recuperaTutteValutazioni();
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:1138>> PrintoValutazioni: " + listVall);
        }
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();
        String jsonVall = gson.toJson(listVall);
        if (DEBUG) {
            System.out.println(">>GUICONTROLLER:1145>> PrintoValutazioni!AjaxAnswer: " + jsonVall);
        }
        out.print(jsonVall);
        out.flush();
    }

    /**
     * Richiesta di eliminare un commento
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws recuperaTutteSollecitazioni
     */
    private void eliminaCommento(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        checkPermission(request, response, "needToBeLogged");
        Object id = request.getParameter("commento_id");
        CommentoCtrl.eliminaCommento(id);
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.print("");
        out.flush();
    }
    
    /**
     * Richiesta di eliminare una sollecitazione
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws recuperaTutteSollecitazioni
     */
    private void eliminaSollecitazione(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException, ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        checkPermission(request, response, "needToBeLogged");
        Object id = request.getParameter("sollecitazione_id");
        if(DEBUG) System.out.println(">>GUICONTROLLER:1215>> eliminaSollecitazione --> sollid"+id);
        SollecitazioneCtrl.eliminaSollecitazione(id);
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.print("");
        out.flush();    }
}