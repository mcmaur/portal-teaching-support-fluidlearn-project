package dbBroker;

import classes.partedinamica.Post;
import java.sql.*;
import java.util.*;
import GUIcontroller.GuiController;

public class PostDB {

    private static Connection conn = null;
    private static Statement st = null;
    private static ResultSet rs = null;

    static private void inizializza()
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        Object newInstance = Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        conn = DriverManager.getConnection("jdbc:derby://localhost:1527/sample", "app", "app");
        st = conn.createStatement();
    }

    static private void chiudi() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (st != null) {
            st.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    /**
     * Retieve all post that are linked to specified course
     *
     * @param nameCourse
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @return list of all posts
     */
    static public List<Post> retrieveAllPostByCourse(String nameCourse) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        inizializza();
        List<Post> postList = new ArrayList<Post>();
        if (GuiController.DEBUG) {
            System.out.println(">>PostsDBInterface:32>> Query retrievePostByCourse: SELECT * FROM POSTS WHERE COURSE_NAME = '" + nameCourse + "'");
        }
        rs = st.executeQuery("SELECT * FROM POSTS WHERE COURSE_NAME = '" + nameCourse + "'");
        while (rs.next()) {
            Post pst = new Post();
            pst.setId(rs.getInt(1));
            pst.setId_corso(rs.getInt(2));
            pst.setNome_corso(rs.getString(3));
            pst.setAttore(rs.getString(4));
            pst.setTesto(rs.getString(5));
            pst.setData(rs.getString(6));
            pst.setOra(rs.getString(7));
            pst.setTitolo(rs.getString(8));
            pst.setId_uda(rs.getInt(9));
            pst.setVisibilita(rs.getString(10));
            pst.setIsBozza(rs.getBoolean(11));
            pst.setPlugin(rs.getString(12));
            pst.setId_artefatto(rs.getInt(13));
            pst.setId_risorsa(rs.getInt(14));
            postList.add(pst);
        }
        chiudi();
        return postList;
    }

    /**
     * Retieve all post that are linked to specified user
     *
     * @param nameUser
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @return list of all posts
     */
    static public List<Post> retrieveAllPostByUser(String nameUser) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        inizializza();
        List<Post> postList = new ArrayList<Post>();
        if (GuiController.DEBUG) {
            System.out.println(">>PostsDBInterface:76>> Query retrieveAllPostByUser: SELECT * FROM POSTS WHERE USER_USERNAME = '" + nameUser + "'");
        }
        rs = st.executeQuery("SELECT * FROM POSTS WHERE AUTORE = '" + nameUser + "'");
        while (rs.next()) {
            Post pst = new Post();
            pst.setId(rs.getInt(1));
            pst.setId_corso(rs.getInt(2));
            pst.setNome_corso(rs.getString(3));
            pst.setAttore(rs.getString(4));
            pst.setTesto(rs.getString(5));
            pst.setData(rs.getString(6));
            pst.setOra(rs.getString(7));
            pst.setTitolo(rs.getString(8));
            pst.setId_uda(rs.getInt(9));
            pst.setVisibilita(rs.getString(10));
            pst.setIsBozza(rs.getBoolean(11));
            pst.setPlugin(rs.getString(12));
            pst.setId_artefatto(rs.getInt(13));
            pst.setId_risorsa(rs.getInt(14));
            postList.add(pst);
        }
        chiudi();
        return postList;
    }

    /**
     * Add a new post
     *
     * @param idPost
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    static public void deletePost(String idPost) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>PostsDBInterface:111>> Query deletePost: DELETE FROM POSTS WHERE ID = " + idPost + "");
        }
        st.executeUpdate("DELETE FROM POSTS WHERE ID = " + idPost + "");
        chiudi();
    }

    /**
     * Switch a post from draft=1 to draft=0
     *
     * @param idPost
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public void savePostDraft(String idPost) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>PostsDBInterface:189>> Query savePostDraft:update POSTS set ISDRAFT=0 where ID=" + idPost);
        }
        st.executeUpdate("UPDATE POSTS SET ISDRAFT=0 WHERE ID=" + idPost);
        chiudi();
    }

    public static void save(Post pst)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        int bozza = 0;
        if (pst.isIsBozza() == true) {
            bozza = 1;
        }
        if (GuiController.DEBUG) {
            System.out.println(">>PostsDBInterface:121>> Query savePost: INSERT INTO POSTS (COURSE_ID,COURSE_NAME,AUTORE,TEXT,DATE,TIME,TITLE,ID_UDA,ID_ALLEG,VISIBILITY,ISDRAFT,PLUGIN,ID_ARTEFACT,RISORSA_ID) VALUES (" + pst.getId_corso() + ", '" + pst.getNome_corso() + "', '" + pst.getAttore() + "', '" + pst.getTesto() + "', '" + pst.getData() + "', '" + pst.getOra() + "', '" + pst.getTitolo() + "', " + pst.getId_uda() + ", " + pst.getId_risorsa() + ", '" + pst.getVisibilita() + "', " + pst.isIsBozza() + ", '" + pst.getPlugin() + "', " + pst.getId_artefatto() + ", " + pst.getId_risorsa() + ")");
        }
        st.executeUpdate("INSERT INTO POSTS (COURSE_ID,COURSE_NAME,AUTORE,TEXT,DATE,TIME,TITLE,ID_UDA,VISIBILITY,ISDRAFT,PLUGIN,ID_ARTEFACT,RISORSA_ID) VALUES (" + pst.getId_corso() + ", '" + pst.getNome_corso() + "', '" + pst.getAttore() + "', '" + pst.getTesto() + "', '" + pst.getData() + "', '" + pst.getOra() + "', '" + pst.getTitolo() + "', " + pst.getId_uda() + ", '" + pst.getVisibilita() + "', " + bozza + ", '" + pst.getPlugin() + "', " + pst.getId_artefatto() + ", " + pst.getId_risorsa() + ")");
        chiudi();
    }

    public static void saveMod(Post pst)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>PostsDBInterface:121>> id: " + pst.getId());
        }
        int bozza = 0;
        if (pst.isIsBozza() == true) {
            bozza = 1;
        }
        if (GuiController.DEBUG) {
            System.out.println(">>PostsDBInterface:121>> Query saveMod: UPDATE POSTS SET COURSE_ID=" + pst.getId_corso() + ",COURSE_NAME='" + pst.getNome_corso() + "',AUTORE='" + pst.getAttore() + "',TEXT='" + pst.getTesto() + "',DATE='" + pst.getData() + "',TIME='" + pst.getOra() + "',TITLE='" + pst.getTitolo() + "',ID_UDA=" + pst.getId_uda() + ",ID_ALLEG=" + pst.getId_risorsa() + ",VISIBILITY='" + pst.getVisibilita() + "',ISDRAFT=" + bozza + ",PLUGIN='" + pst.getPlugin() + "',ID_ARTEFACT=" + pst.getId_artefatto() + ",RISORSA_ID=" + pst.getId_risorsa() + " WHERE ID=" + pst.getId());
        }
        st.executeUpdate("UPDATE POSTS SET COURSE_ID=" + pst.getId_corso() + ",COURSE_NAME='" + pst.getNome_corso() + "',AUTORE='" + pst.getAttore() + "',TEXT='" + pst.getTesto() + "',DATE='" + pst.getData() + "',TIME='" + pst.getOra() + "',TITLE='" + pst.getTitolo() + "',ID_UDA=" + pst.getId_uda() + ",ID_ALLEG=" + pst.getId_risorsa() + ",VISIBILITY='" + pst.getVisibilita() + "',ISDRAFT=" + bozza + ",PLUGIN='" + pst.getPlugin() + "',ID_ARTEFACT=" + pst.getId_artefatto() + ",RISORSA_ID=" + pst.getId_risorsa() + " WHERE ID=" + pst.getId());
        chiudi();
    }

    public static void load(Post pst)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>PostsDBInterface:145>> Query load: SELECT * FROM POSTS WHERE ID = " + pst.getId());
        }
        rs = st.executeQuery("SELECT * FROM POSTS WHERE ID = " + pst.getId() + "");
        while (rs.next()) {
            pst.setId(rs.getInt(1));
            pst.setId_corso(rs.getInt(2));
            pst.setNome_corso(rs.getString(3));
            pst.setAttore(rs.getString(4));
            pst.setTesto(rs.getString(5));
            pst.setData(rs.getString(6));
            pst.setOra(rs.getString(7));
            pst.setTitolo(rs.getString(8));
            pst.setId_uda(rs.getInt(9));
            pst.setVisibilita(rs.getString(10));
            pst.setIsBozza(rs.getBoolean(11));
            pst.setPlugin(rs.getString(12));
            pst.setId_artefatto(rs.getInt(13));
            pst.setId_risorsa(rs.getInt(14));
        }
        chiudi();
    }

    /**
     * Switch a post from draft=0 to draft=1
     *
     * @param idPost
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public void transformaInBozza(String idPost) 
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>PostsDBInterface:219>> Query transformaInBozza:update POSTS set ISDRAFT=1 where ID=" + idPost);
        }
        st.executeUpdate("UPDATE POSTS SET ISDRAFT=1 WHERE ID=" + idPost);
        chiudi();
    }

}