package dbBroker;

import GUIcontroller.GuiController;
import classes.partedinamica.Sollecitazione;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SollecitazioneDB {

    private static Connection conn = null;
    private static Statement st = null;
    private static ResultSet rs = null;

    static private void inizializza()
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        Object newInstance = Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        conn = DriverManager.getConnection("jdbc:derby://localhost:1527/sample", "app", "app");
        st = conn.createStatement();
    }

    static private void chiudi() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (st != null) {
            st.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    public static void save(Sollecitazione soll)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        if (GuiController.DEBUG) {
                System.out.println(">>SollecitazioneDB:40>> dataScad: " + soll.getDataScadenza());
            }
        int bozza = 0;
        if (soll.isIsBozza() == true) {
            bozza = 1;
        }
        int usaPlg = 0;
        if (soll.isUsaStessoPlugin() == true) {
            usaPlg = 1;
        }
        if (GuiController.DEBUG) {
            System.out.println(">>SollecitazioneDB:47>> Query save: INSERT INTO SOLLECITAZIONI (CORSO_ID,CORSO_NOME,AUTORE,TESTO,DATAPUB,TIMEPUB,TITOLO,UDA_ID,VISIBILITA,ISBOZZA,PLUGIN,ARTEFATTO_ID,RISORSA_ID,DATASCADENZA,USASTESSOPLUGIN,VISIBILITARISPOSTE,VOTOMAX) VALUES "
                    + "(" + soll.getId_corso() + ", '" + soll.getNome_corso() + "', '" + soll.getAttore() + "', '" + soll.getTesto() + "',"
                    + " '" + soll.getData() + "', '" + soll.getOra() + "', '" + soll.getTitolo() + "', " + soll.getId_uda() + ", "
                    + "'" + soll.getVisibilita() + "', " + bozza + ", '" + soll.getPlugin() + "', "
                    + "" + soll.getId_artefatto() + ", " + soll.getId_risorsa() + ", '" + soll.getDataScadenza() + "', " + usaPlg+ ", '" + soll.getVisibilitaRisposte() + "', '" + soll.getVotoMax()+ "')");
        }
        st.executeUpdate("INSERT INTO SOLLECITAZIONI (CORSO_ID,CORSO_NOME,AUTORE,TESTO,DATAPUB,TIMEPUB,TITOLO,UDA_ID,VISIBILITA,ISBOZZA,PLUGIN,ARTEFATTO_ID,RISORSA_ID,DATASCADENZA,USASTESSOPLUGIN,VISIBILITARISPOSTE,VOTOMAX) VALUES "
                + "(" + soll.getId_corso() + ", '" + soll.getNome_corso() + "', '" + soll.getAttore() + "', '" + soll.getTesto() + "',"
                + " '" + soll.getData() + "', '" + soll.getOra() + "', '" + soll.getTitolo() + "', " + soll.getId_uda() + ", "
                + "'" + soll.getVisibilita() + "', " + bozza + ", '" + soll.getPlugin() + "', "
                + "" + soll.getId_artefatto() + ", " + soll.getId_risorsa() + ", '" + soll.getDataScadenza() + "', " + usaPlg+ ", '" + soll.getVisibilitaRisposte() + "', '" + soll.getVotoMax()+ "')");
        chiudi();
    }

    public static List<Sollecitazione> recuperaTutteSollecitazione(Integer corso_id) throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        List<Sollecitazione> sollecitazioniList = new ArrayList<Sollecitazione>();
        if(corso_id==null){
            if (GuiController.DEBUG)System.out.println(">>PostsDBInterface:121>> Query recuperaTutteSollecitazione: SELECT * FROM SOLLECITAZIONI");
            rs = st.executeQuery("SELECT * FROM SOLLECITAZIONI");
        }
        else{
            if (GuiController.DEBUG)System.out.println(">>PostsDBInterface:121>> Query recuperaTutteSollecitazione: SELECT * FROM SOLLECITAZIONI WHERE CORSO_ID = "+corso_id);
            rs = st.executeQuery("SELECT * FROM SOLLECITAZIONI WHERE CORSO_ID = "+corso_id);
        }
        while (rs.next()) {
            Sollecitazione sllc = new Sollecitazione();
            sllc.setId(rs.getInt(1));
            sllc.setId_artefatto(rs.getInt(2));
            sllc.setTitolo(rs.getString(3));
            sllc.setTesto(rs.getString(4));
            sllc.setAttore(rs.getString(5));
            sllc.setPlugin(rs.getString(6));
            sllc.setId_uda(rs.getInt(7));
            sllc.setIsBozza(rs.getBoolean(8));
            sllc.setVisibilita(rs.getString(9));
            sllc.setData(rs.getString(10));
            sllc.setOra(rs.getString(11));
            sllc.setDataScadenza(rs.getString(12));
            sllc.setId_risorsa(rs.getInt(13));
            sllc.setId_corso(rs.getInt(14));
            sllc.setNome_corso(rs.getString(15));
            sllc.setUsaStessoPlugin(rs.getBoolean(16));
            sllc.setVisibilitaRisposte(rs.getString(17));
            sllc.setVotoMax(rs.getString(18));
            sollecitazioniList.add(sllc);
        }
        chiudi();
        return sollecitazioniList;
    }     

    public static void load(Sollecitazione soll)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>SollecitazioneDB:103>> Query load: SELECT * FROM SOLLECITAZIONI WHERE ID = " + soll.getId());
        }
        rs = st.executeQuery("SELECT * FROM SOLLECITAZIONI WHERE ID = " + soll.getId() + "");
        while (rs.next()) {
            soll.setId(rs.getInt(1));
            soll.setId_artefatto(rs.getInt(2));
            soll.setTitolo(rs.getString(3));
            soll.setTesto(rs.getString(4));
            soll.setAttore(rs.getString(5));
            soll.setPlugin(rs.getString(6));
            soll.setId_uda(rs.getInt(7));
            soll.setIsBozza(rs.getBoolean(8));
            soll.setVisibilita(rs.getString(9));
            soll.setData(rs.getString(10));
            soll.setOra(rs.getString(11));
            soll.setDataScadenza(rs.getString(12));
            soll.setId_risorsa(rs.getInt(13));
            soll.setId_corso(rs.getInt(14));
            soll.setNome_corso(rs.getString(15));
            soll.setUsaStessoPlugin(rs.getBoolean(16));
            soll.setVisibilitaRisposte(rs.getString(17));
            soll.setVotoMax(rs.getString(18));
        }
        chiudi();
    }
    
    public static void saveMod(Sollecitazione pst)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>SollecitazioneDB:130>> id: " + pst.getId());
        }
        int bozza = 0;
        if (pst.isIsBozza() == true) {
            bozza = 1;
        }
        if (GuiController.DEBUG) {
            System.out.println(">>SollecitazioneDB:137>> Query saveMod: UPDATE SOLLECITAZIONI SET CORSO_ID=" + pst.getId_corso() + ",CORSO_NOME='" + pst.getNome_corso() + "',AUTORE='" + pst.getAttore() + "',TESTO='" + pst.getTesto() + "',DATAPUB='" + pst.getData() + "',TIMEPUB='" + pst.getOra() + "',TITOLO='" + pst.getTitolo() + "',UDA_ID=" + pst.getId_uda() + ",VISIBILITA='" + pst.getVisibilita() + "',ISBOZZA=" + bozza + ",PLUGIN='" + pst.getPlugin() + "',ARTEFATTO_ID=" + pst.getId_artefatto() + ",RISORSA_ID=" + pst.getId_risorsa() + ",DATASCADENZA='"+pst.getDataScadenza() +"' WHERE ID=" + pst.getId());
        }
        st.executeUpdate("UPDATE SOLLECITAZIONI SET CORSO_ID=" + pst.getId_corso() + ",CORSO_NOME='" + pst.getNome_corso() + "',AUTORE='" + pst.getAttore() + "',TESTO='" + pst.getTesto() + "',DATAPUB='" + pst.getData() + "',TIMEPUB='" + pst.getOra() + "',TITOLO='" + pst.getTitolo() + "',UDA_ID=" + pst.getId_uda() + ",VISIBILITA='" + pst.getVisibilita() + "',ISBOZZA=" + bozza + ",PLUGIN='" + pst.getPlugin() + "',ARTEFATTO_ID=" + pst.getId_artefatto() + ",RISORSA_ID=" + pst.getId_risorsa() + ",DATASCADENZA='"+pst.getDataScadenza() +"' WHERE ID=" + pst.getId());
        chiudi();
    }

    public static void eliminaSollecitazione(Integer id) 
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>SollecitazioneDB:155>> Query eliminaSollecitazione: DELETE FROM SOLLECITAZIONI WHERE ID = " + id + "");
        }
        st.executeUpdate("DELETE FROM SOLLECITAZIONI WHERE ID = " + id + "");
        chiudi();
    }
    
}