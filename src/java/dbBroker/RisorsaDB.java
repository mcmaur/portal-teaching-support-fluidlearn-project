package dbBroker;

import GUIcontroller.GuiController;
import classes.Risorsa;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class RisorsaDB {

    private static Connection conn = null;
    private static Statement st = null;
    private static ResultSet rs = null;

    static private void inizializza()
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        Object newInstance = Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        conn = DriverManager.getConnection("jdbc:derby://localhost:1527/sample", "app", "app");
        st = conn.createStatement();
    }

    static private void chiudi() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (st != null) {
            st.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    public static void load(Risorsa ris)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>RisorsaDB:33> Query load: SELECT * FROM RISORSE WHERE ID = " + ris.getId());
        }
        rs = st.executeQuery("SELECT * FROM RISORSE WHERE ID = " + ris.getId()+ "");
        while (rs.next()) {
            ris.setNome(rs.getString(2));
            ris.setUrl(rs.getString(3));
        }
        chiudi();
    }
}
