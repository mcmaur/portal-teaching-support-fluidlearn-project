package dbBroker;

import GUIcontroller.GuiController;
import classes.Valutazione;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ValutazioneDB {
    private static Connection conn = null;
    private static Statement st = null;
    private static ResultSet rs = null;

    static private void inizializza()
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        Object newInstance = Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        conn = DriverManager.getConnection("jdbc:derby://localhost:1527/sample", "app", "app");
        st = conn.createStatement();
    }

    static private void chiudi() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (st != null) {
            st.close();
        }
        if (rs != null) {
            rs.close();
        }
    }
    
    /**
     * Salva una nuova risposta
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws SQLException
     * @throws IllegalAccessException
     */
    public static void salva(Valutazione ris)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>ValutazioneDB:47>> QUERY SALVA: INSERT INTO VALUTAZIONI (RISPOSTA_ID,ESAMINATORE,AUTORE_RISP,NOTA,VOTO,CORSO_ID,CORSO_NOME) "
                + "VALUES (" + ris.getRisposta_id() + ", '" + ris.getEsaminatore() + "', '" + ris.getAutore_risp()+ "', '" + ris.getNota()+ "', '" + ris.getVoto()+ "', " + ris.getId_corso()+ ", '" + ris.getNome_corso()+"')");
        }
        st.executeUpdate("INSERT INTO VALUTAZIONI (RISPOSTA_ID,ESAMINATORE,AUTORE_RISP,NOTA,VOTO,CORSO_ID,CORSO_NOME) "
                + "VALUES (" + ris.getRisposta_id() + ", '" + ris.getEsaminatore() + "', '" + ris.getAutore_risp()+ "', '" + ris.getNota()+ "', '" + ris.getVoto()+ "', " + ris.getId_corso()+ ", '" + ris.getNome_corso()+"')");
        chiudi();
    }

    /**
     * Recupera tutte le valutazioni
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @return list of all posts
     */
    public static List<Valutazione> recuperaTutteValutazioni() throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        List <Valutazione> valutazList = new ArrayList<Valutazione>();
        if (GuiController.DEBUG) {
            System.out.println(">>ValutazioneDB:32>> Query retrievePostByCourse: SELECT * VALUTAZIONI");
        }
        rs = st.executeQuery("SELECT * FROM VALUTAZIONI");
        while (rs.next()) {
            Valutazione val = new Valutazione();
            val.setId(rs.getInt(1));
            val.setRisposta_id(rs.getInt(2));
            val.setEsaminatore(rs.getString(3));
            val.setAutore_risp(rs.getString(4));
            val.setNota(rs.getString(5));
            val.setVoto(rs.getString(6));
            val.setId_corso(rs.getInt(7));
            val.setNome_corso(rs.getString(8));
            valutazList.add(val);
        }
        chiudi();
        return valutazList;
    }    

    public static Integer getMaximumId()
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        int max=0;
        rs = st.executeQuery("SELECT MAX (ID) FROM VALUTAZIONI");
        while (rs.next()) {
            max=rs.getInt(1);
        }
        if (GuiController.DEBUG) {
            System.out.println(">>ValutazioneDB:97>> max id: "+max);
        }
        chiudi();
        return max;
    }
}