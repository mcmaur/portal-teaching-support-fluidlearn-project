package dbBroker;

import GUIcontroller.GuiController;
import classes.partedinamica.Risposta;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class RispostaDB {

    private static Connection conn = null;
    private static Statement st = null;
    private static ResultSet rs = null;

    static private void inizializza()
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        Object newInstance = Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        conn = DriverManager.getConnection("jdbc:derby://localhost:1527/sample", "app", "app");
        st = conn.createStatement();
    }

    static private void chiudi() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (st != null) {
            st.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    /**
     * Salva una nuova risposta
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws SQLException
     * @throws IllegalAccessException
     */
    public static void salva(Risposta ris)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>RispostaDB:49>> QUERY SALVA: INSERT INTO RISPOSTE (RISORSA_ID,AUTORE,TESTO,SOLLECITAZIONE_ID,PLUGIN,VALUTAZIONE_ID,ARTEFATTO_ID) "
                + "VALUES (" + ris.getId_risorsa() + ", '" + ris.getAttore() + "', '" + ris.getTesto() + "', " + ris.getId_contributo() + ", '" + ris.getPlugin() + "', " + ris.getId_valutazione() + ", " + ris.getId_artefatto() + ")");
        }
        st.executeUpdate("INSERT INTO RISPOSTE (RISORSA_ID,AUTORE,TESTO,SOLLECITAZIONE_ID,PLUGIN,VALUTAZIONE_ID,ARTEFATTO_ID) "
                + "VALUES (" + ris.getId_risorsa() + ", '" + ris.getAttore() + "', '" + ris.getTesto() + "', " + ris.getId_contributo() + ", '" + ris.getPlugin() + "', " + ris.getId_valutazione() + ", " + ris.getId_artefatto() + ")");
        chiudi();
    }
    
    /**
     * Carica un commento dal database
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws SQLException
     * @throws IllegalAccessException
     */
    public static void load(Risposta ris)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>RispostaDB:71>> QUERY LOAD: SELECT * FROM RISPOSTE WHERE ID="+ris.getId());
        }
        rs = st.executeQuery("SELECT * FROM RISPOSTE WHERE ID="+ris.getId());
        while (rs.next()) {
            ris.setId_risorsa(rs.getInt(2));
            ris.setAttore(rs.getString(3));
            ris.setTesto(rs.getString(4));
            ris.setId_contributo(rs.getInt(5));
            ris.setPlugin(rs.getString(6));
            ris.setId_valutazione(rs.getInt(7));
            ris.setId_artefatto(rs.getInt(8));
        }
        chiudi();
    }
    
    /**
     * Salva una modifica di una risposta
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws SQLException
     * @throws IllegalAccessException
     */
    public static void salvaMod(Risposta ris)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>RispostaDB:49>> QUERY SALVAMOD: UPDATE RISPOSTE SET RISORSA_ID="+ris.getId_risorsa()+",AUTORE='" + ris.getAttore() + "',TESTO='" + ris.getTesto() + "',SOLLECITAZIONE_ID=" + ris.getId_contributo() + ",PLUGIN='" + ris.getPlugin() + "',VALUTAZIONE_ID=" + ris.getId_valutazione() + ",ARTEFATTO_ID=" + ris.getId_artefatto() + " WHERE ID=" + ris.getId());
        }
        st.executeUpdate("UPDATE RISPOSTE SET RISORSA_ID="+ris.getId_risorsa()+",AUTORE='" + ris.getAttore() + "',TESTO='" + ris.getTesto() + "',SOLLECITAZIONE_ID=" + ris.getId_contributo() + ",PLUGIN='" + ris.getPlugin() + "',VALUTAZIONE_ID=" + ris.getId_valutazione() + ",ARTEFATTO_ID=" + ris.getId_artefatto() + " WHERE ID=" + ris.getId());
        chiudi();
    }
    
    /**
     * Retieve all post that are linked to specified course
     *
     * @param nameCourse
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @return list of all posts
     */
    static public List <Risposta> recuperaRisposte(String id_sollecitazone) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        inizializza();
        List <Risposta> risList = new ArrayList<Risposta>();
        if (GuiController.DEBUG) {
            System.out.println(">>RispostaDB:72>> Query recuperaRisposte: SELECT * FROM RISPOSTE WHERE SOLLECITAZIONE_ID = " + id_sollecitazone + "");
        }
        rs = st.executeQuery("SELECT * FROM RISPOSTE WHERE SOLLECITAZIONE_ID = " + id_sollecitazone + "");
        while (rs.next()) {
            Risposta ris = new Risposta();
            ris.setId(rs.getInt(1));
            ris.setId_risorsa(rs.getInt(2));
            ris.setAttore(rs.getString(3));
            ris.setTesto(rs.getString(4));
            ris.setId_contributo(rs.getInt(5));
            ris.setPlugin(rs.getString(6));
            ris.setId_valutazione(rs.getInt(7));
            ris.setId_artefatto(rs.getInt(8));
            risList.add(ris);
        }
        chiudi();
        return risList;
    }
}
