package dbBroker;

import classes.Utente;
import java.sql.*;
import GUIcontroller.GuiController;

public class UtenteDB {

    static final String url = "jdbc:derby://localhost:1527/sample";
    static final String user_db = "app";
    static final String pwd_db = "app";

    /**
     * Check if username and password are into the DB
     *
     * @param usr
     * @param pwd
     * @return true if present , false otherwise
     */
    static public boolean checkLogin(String usr, String passwd) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        boolean userPresent = false;

        try {
            Object newInstance = Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            conn = DriverManager.getConnection(url, user_db, pwd_db);
            st = conn.createStatement();
            if (GuiController.DEBUG) {
                System.out.println(">>UsersDBInterface:29>> Query checkLogin: SELECT * FROM USERS WHERE USERNAME='" + usr + "' AND PASSWORD='" + passwd + "'");
            }
            rs = st.executeQuery("SELECT * FROM USERS WHERE USERNAME='" + usr + "' AND PASSWORD='" + passwd + "'");
            int count = 0;
            while (rs.next()) {
                count++;
            }
            if (count > 0) {
                userPresent = true;
            }
        } catch (SQLException e) {
            return userPresent;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException sqle) {
                return userPresent;
            }
        }
        return userPresent;
    }

    /**
     * Add an user to the Users table in DB
     *
     * @param usr
     * @param pwd
     * @return true if adding new user goes right , false otherwise
     */
    static public boolean addUser(String usr, String pwd, String email) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Connection conn = null;
        Statement st = null;
        boolean addingPossible = false;
        try {
            Object newInstance = Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            conn = DriverManager.getConnection(url, user_db, pwd_db);
            st = conn.createStatement();
            if (GuiController.DEBUG) {
                System.out.println(">>UsersDBInterface:71>> Query addUser: INSERT INTO USERS VALUES ('" + usr + "','" + pwd + "','" + email + "')");
            }
            st.executeUpdate("INSERT INTO USERS VALUES ('" + usr + "','" + pwd + "','" + email + "')");
            addingPossible = true;
        } catch (SQLException e) {
            return false;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (st != null) {
                    st.close();
                }
            } catch (SQLException sqle) {
                return false;
            }
        }
        return addingPossible;
    }

    /**
     * Request for personal info of an user
     *
     * @param usr
     * @return an User object containing all user info
     */
    static public Utente retrievePersonalInfo(String usr) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        Utente userFound = null;
        try {
            Object newInstance = Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            conn = DriverManager.getConnection(url, user_db, pwd_db);
            st = conn.createStatement();
            if (GuiController.DEBUG) {
                System.out.println(">>UsersDBInterface:107>> Query requestPersonalInfo: SELECT * FROM USERS WHERE USERNAME='" + usr + "'");
            }
            rs = st.executeQuery("SELECT * FROM USERS WHERE USERNAME='" + usr + "'");
            while (rs.next()) {
                userFound = new Utente(rs.getString(2), rs.getString(3), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10));
            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException sqle) {
                return null;
            }
        }
        return userFound;
    }
}