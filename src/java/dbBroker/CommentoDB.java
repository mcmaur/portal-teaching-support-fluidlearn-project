package dbBroker;

import GUIcontroller.GuiController;
import java.sql.*;
import java.util.List;
import classes.partedinamica.Commento;
import java.util.ArrayList;

public class CommentoDB {

    private static Connection conn = null;
    private static Statement st = null;
    private static ResultSet rs = null;

    static private void inizializza()
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        Object newInstance = Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        conn = DriverManager.getConnection("jdbc:derby://localhost:1527/sample", "app", "app");
        st = conn.createStatement();
    }

    static private void chiudi() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        if (st != null) {
            st.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    /**
     * Retieve all comments that are into the DB
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @return list of all courses
     */
    static public List<Commento> retrieveAllComments()
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        inizializza();
        List<Commento> commentsList = new ArrayList<Commento>();
        if (GuiController.DEBUG) {
            System.out.println(">>CommentsDBInterface:71>> Query retrieveAllComments: SELECT * FROM COMMENTS ");
        }
        rs = st.executeQuery("SELECT * FROM COMMENTS");
        while (rs.next()) {
            Commento cmt = new Commento();
            cmt.setId(rs.getInt(1));
            cmt.setId_contributo(rs.getInt(2));
            cmt.setAttore(rs.getString(3));
            cmt.setTesto(rs.getString(4));
            cmt.setId_risorsa(rs.getInt(2));
            commentsList.add(cmt);
        }
        chiudi();
        return commentsList;
    }

    /**
     * Delete all comments that refers to give post_id
     *
     * @param post_id
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @return list of all courses
     */
    static public void deleteCommentsByPostID(String post_id)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>CommentsDBInterface:114>> Query deleteCommentsByPostID: DELETE FROM COMMENTS WHERE POST_ID = " + post_id + "");
        }
        st.executeUpdate("DELETE FROM COMMENTS WHERE POST_ID = " + post_id + "");
        chiudi();
    }

    /**
     * Salva un commento nel database
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws SQLException
     * @throws IllegalAccessException
     */
    public static void salva(Commento cmt)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>CommentsDBInterface:114>> QUERY SALVA: INSERT INTO COMMENTS (POST_ID,USER_USERNAME,TEXT,RESOURCE_ID) VALUES (" + cmt.getId_contributo() + ", '" + cmt.getAttore() + "', '" + cmt.getTesto() + "', " + cmt.getId_risorsa() + ")");
        }
        st.executeUpdate("INSERT INTO COMMENTS (POST_ID,USER_USERNAME,TEXT,RESOURCE_ID) VALUES (" + cmt.getId_contributo() + ", '" + cmt.getAttore() + "', '" + cmt.getTesto() + "', " + cmt.getId_risorsa() + ")");
        chiudi();
    }
    
    /**
     * Carica un commento dal database
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws SQLException
     * @throws IllegalAccessException
     */
    public static void load(Commento cmt)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>CommentsDBInterface:112>> QUERY LOAD: SELECT * FROM COMMENTS WHERE ID="+cmt.getId());
        }
        rs = st.executeQuery("SELECT * FROM COMMENTS WHERE ID="+cmt.getId());
        while (rs.next()) {
            cmt.setId_contributo(rs.getInt(2));
            cmt.setAttore(rs.getString(3));
            cmt.setTesto(rs.getString(4));
            cmt.setId_risorsa(rs.getInt(5));
        }
        chiudi();
    }
    
    public static Integer getMaximumId()
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        inizializza();
        int max=0;
        rs = st.executeQuery("SELECT MAX (ID) FROM COMMENTS");
        while (rs.next()) {
            max=rs.getInt(1);
        }
        if (GuiController.DEBUG) {
            System.out.println(">>CommentsDB:132>> max id: "+max);
        }
        chiudi();
        return max;
    }
    
    /**
     * Delete comment by id
     *
     * @param id
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @return list of all courses
     */
    static public void deleteCommentsByID(Integer id)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        inizializza();
        if (GuiController.DEBUG) {
            System.out.println(">>CommentsDBInterface:114>> Query deleteCommentsByPostID: DELETE FROM COMMENTS WHERE ID = " + id + "");
        }
        st.executeUpdate("DELETE FROM COMMENTS WHERE ID = " + id + "");
        chiudi();
    }
}
