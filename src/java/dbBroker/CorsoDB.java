package dbBroker;

import classes.Corso;
import java.sql.*;
import java.util.*;
import GUIcontroller.GuiController;

public class CorsoDB {

    static final String url = "jdbc:derby://localhost:1527/sample";
    static final String user_db = "app";
    static final String pwd_db = "app";

    /**
     * Retieve all courses that are into the DB
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @return list of all courses
     */
    static public List<Corso> retrieveAllCourses()
            throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        List<Corso> coursesList = new ArrayList<Corso>();

        try {
            Object newInstance = Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            conn = DriverManager.getConnection(url, user_db, pwd_db);
            st = conn.createStatement();
            if (GuiController.DEBUG) {
                System.out.println(">>CoursesDBInterface:32>> Query sendAllCourses: SELECT * FROM COURSES ");
            }
            rs = st.executeQuery("SELECT * FROM COURSES ");
            while (rs.next()) {
                coursesList.add(new Corso(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException sqle) {
                return null;
            }
        }
        return coursesList;
    }

    /**
     * Retieve all courses that are into the DB
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @return list of all courses
     */
    static public List<Corso> retrieveCoursesByYear(String year)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        List<Corso> coursesList = new ArrayList<Corso>();

        try {
            Object newInstance = Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            conn = DriverManager.getConnection(url, user_db, pwd_db);
            st = conn.createStatement();
            if (GuiController.DEBUG) {
                System.out.println(">>CoursesDBInterface:76>> Query retrieveCoursesByYear: SELECT * FROM COURSES WHERE YEARS='" + year + "'");
            }
            rs = st.executeQuery("SELECT * FROM COURSES WHERE YEARS='" + year + "'");
            while (rs.next()) {
                coursesList.add(new Corso(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException sqle) {
                return null;
            }
        }
        return coursesList;
    }

    /**
     * Retieve all courses that are into the DB
     *
     * @param CourseName - name of the course
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @return list of all courses
     */
    static public Corso retrieveCourseInfo(String CourseName)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        Corso result = null;

        try {
            Object newInstance = Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            conn = DriverManager.getConnection(url, user_db, pwd_db);
            st = conn.createStatement();
            if (GuiController.DEBUG) {
                System.out.println(">>CoursesDBInterface121>> Query retrieveCourseInfo: SELECT * FROM COURSES WHERE NAME='" + CourseName + "'");
            }
            rs = st.executeQuery("SELECT * FROM COURSES WHERE NAME='" + CourseName + "'");
            while (rs.next()) {
                result = new Corso(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
        } catch (SQLException e) {
            return null;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException sqle) {
                return null;
            }
        }
        return result;
    }
}