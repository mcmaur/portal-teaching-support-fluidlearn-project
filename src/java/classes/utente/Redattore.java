package classes.utente;

public class Redattore extends Ruolo{
	Partecipante part;

	public Redattore(Partecipante p) {
		this.part = p;
	}

	public String getDescription() {
		return part.getDescription() + ", Redattore";
	}

}