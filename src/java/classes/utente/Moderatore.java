package classes.utente;

public class Moderatore extends Ruolo{
	Partecipante part;

	public Moderatore(Partecipante p) {
		this.part = p;
	}

	public String getDescription() {
		return part.getDescription() + ", Moderatore";
	}

}
