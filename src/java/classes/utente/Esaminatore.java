package classes.utente;

public class Esaminatore extends Ruolo{
	Partecipante part;

	public Esaminatore(Partecipante p) {
		this.part = p;
	}

	public String getDescription() {
		return part.getDescription() + ", Esaminatore";
	}

}
