package classes.utente;

public class Tutor extends Ruolo{
	Partecipante part;

	public Tutor(Partecipante p) {
		this.part = p;
	}

	public String getDescription() {
		return part.getDescription() + ", Tutor";
	}

}