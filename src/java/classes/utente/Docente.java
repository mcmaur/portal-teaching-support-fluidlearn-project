package classes.utente;

public class Docente extends Ruolo{
	Partecipante part;

	public Docente(Partecipante p) {
		this.part = p;
	}

	public String getDescription() {
		return part.getDescription() + ", Docente";
	}

}
