package classes.utente;

public class Studente extends Ruolo{
	Partecipante part;

	public Studente(Partecipante p) {
		this.part = p;
	}

	public String getDescription() {
		return part.getDescription() + ", Studente";
	}

}
