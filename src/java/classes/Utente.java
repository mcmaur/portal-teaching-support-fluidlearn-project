package classes;

public class Utente {

    private String name, surname, email, address, telephone, year_registration, career_situation, photo_path;

    public Utente(String name, String surname, String email, String address, String telephone, String year_registration, String career_situation, String photo_path) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.address = address;
        this.telephone = telephone;
        this.year_registration = year_registration;
        this.career_situation = career_situation;
        this.photo_path = photo_path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getYear_registration() {
        return year_registration;
    }

    public void setYear_registration(String year_registration) {
        this.year_registration = year_registration;
    }

    public String getCareer_situation() {
        return career_situation;
    }

    public void setCareer_situation(String career_situation) {
        this.career_situation = career_situation;
    }

    public String getPhoto_path() {
        return photo_path;
    }

    public void setPhoto_path(String photo_path) {
        this.photo_path = photo_path;
    }

    @Override
    public String toString() {
        return "User{" + "name=" + name + ", surname=" + surname + ", email=" + email + ", address=" + address + ", telephone=" + telephone + ", year_registration=" + year_registration + ", career_situation=" + career_situation + ", photo_path=" + photo_path + '}';
    }
}
