package classes.partedinamica;

import java.sql.SQLException;
import GUIcontroller.GuiController;

public class Post extends Contributo {

    private String titolo, data, ora, plugin, visibilita, nome_corso;
    private boolean isBozza;
    private Integer id_corso, id_uda, id_artefatto;

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getOra() {
        return ora;
    }

    public void setOra(String ora) {
        this.ora = ora;
    }

    public String getPlugin() {
        return plugin;
    }

    public void setPlugin(String plugin) {
        this.plugin = plugin;
    }

    public String getVisibilita() {
        return visibilita;
    }

    public void setVisibilita(String visibilita) {
        this.visibilita = visibilita;
    }

    public String getNome_corso() {
        return nome_corso;
    }

    public void setNome_corso(String nome_corso) {
        this.nome_corso = nome_corso;
    }

    public boolean isIsBozza() {
        return isBozza;
    }

    public void setIsBozza(boolean isBozza) {
        this.isBozza = isBozza;
    }

    public Integer getId_corso() {
        return id_corso;
    }

    public void setId_corso(Integer id_corso) {
        this.id_corso = id_corso;
    }

    public Integer getId_uda() {
        return id_uda;
    }

    public void setId_uda(Integer id_uda) {
        this.id_uda = id_uda;
    }

    public Integer getId_artefatto() {
        return id_artefatto;
    }

    public void setId_artefatto(Integer id_artefatto) {
        this.id_artefatto = id_artefatto;
    }

    public void save() throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        dbBroker.PostDB.save(this);
    }

    public void saveMod()
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        dbBroker.PostDB.saveMod(this);
    }

    public void load() throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        dbBroker.PostDB.load(this);
        if (GuiController.DEBUG) {
            System.out.println(">>POST.jv:97<< post:" + this);
        }
    }

    @Override
    public String toString() {
        return super.toString().concat("Post{" + "titolo=" + titolo + ", data=" + data + ", ora=" + ora + ", plugin=" + plugin + ", visibilita=" + visibilita + ", nome_corso=" + nome_corso + ", isBozza=" + isBozza + ", id_corso=" + id_corso + ", id_uda=" + id_uda + ", id_artefatto=" + id_artefatto + '}');
    }
}