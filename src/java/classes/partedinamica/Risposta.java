package classes.partedinamica;

import java.sql.SQLException;

public class Risposta extends Commento{

    String plugin;
    Integer id_artefatto,id_valutazione;

    public String getPlugin() {
        return plugin;
    }

    public void setPlugin(String plugin) {
        this.plugin = plugin;
    }

    public Integer getId_valutazione() {
        return id_valutazione;
    }

    public void setId_valutazione(Integer id_valutazione) {
        this.id_valutazione = id_valutazione;
    }

    public Integer getId_artefatto() {
        return id_artefatto;
    }

    public void setId_artefatto(Integer id_artefatto) {
        this.id_artefatto = id_artefatto;
    }

    @Override
    public String toString() {
        return super.toString().concat("Risposta{" + "plugin=" + plugin + ", id_valutazione=" + id_valutazione + ", id_artefatto=" + id_artefatto + '}');
    }
    
    public void salva() throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        dbBroker.RispostaDB.salva(this);
    }
    
    public void salvaMod() throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        dbBroker.RispostaDB.salvaMod(this);
    }
    
    public void load() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        dbBroker.RispostaDB.load(this);
    }
}
