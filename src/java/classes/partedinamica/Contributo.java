package classes.partedinamica;

abstract class Contributo {

    Integer id;
    Integer id_risorsa;
    String attore;
    String testo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_risorsa() {
        return id_risorsa;
    }

    public void setId_risorsa(Integer id_risorsa) {
        this.id_risorsa = id_risorsa;
    }

    public String getAttore() {
        return attore;
    }

    public void setAttore(String attore) {
        this.attore = attore;
    }

    public String getTesto() {
        return testo;
    }

    public void setTesto(String testo) {
        this.testo = testo;
    }

    @Override
    public String toString() {
        return "Contributo{" + "id=" + getId() + ", id_risorsa=" + getId_risorsa() + ", attore=" + getAttore() + ", testo=" + getTesto() + '}';
    }
}