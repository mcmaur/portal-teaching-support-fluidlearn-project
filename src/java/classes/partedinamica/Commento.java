package classes.partedinamica;

import java.sql.SQLException;

public class Commento extends Contributo {

    Integer id_contributo;

    public Integer getId_contributo() {
        return id_contributo;
    }

    public void setId_contributo(Integer id_contributo) {
        this.id_contributo = id_contributo;
    }

    public void save() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        dbBroker.CommentoDB.salva(this);
    }
    
    public void load() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        dbBroker.CommentoDB.load(this);
    }

    @Override
    public String toString() {
        return super.toString().concat("Commento{" + "id_post=" + id_contributo + '}');
    }
}
