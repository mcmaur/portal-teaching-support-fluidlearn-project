package classes.partedinamica;

import GUIcontroller.GuiController;
import java.sql.SQLException;

public class Sollecitazione extends Post {

    private String dataScadenza, visibilitaRisposte, votoMax;
    private boolean usaStessoPlugin;

    public String getVisibilitaRisposte() {
        return visibilitaRisposte;
    }

    public void setVisibilitaRisposte(String visibilitaRisposte) {
        this.visibilitaRisposte = visibilitaRisposte;
    }

    public String getDataScadenza() {
        return dataScadenza;
    }

    public void setDataScadenza(String dataScadenza) {
        this.dataScadenza = dataScadenza;
    }

    public String getVotoMax() {
        return votoMax;
    }

    public void setVotoMax(String votoMax) {
        this.votoMax = votoMax;
    }

    public boolean isUsaStessoPlugin() {
        return usaStessoPlugin;
    }

    public void setUsaStessoPlugin(boolean usaStessoPlugin) {
        this.usaStessoPlugin = usaStessoPlugin;
    }

    @Override
    public String toString() {
        return super.toString().concat("Sollecitazione{" + "dataScadenza=" + dataScadenza + ", visibilitaRisposte=" + visibilitaRisposte + ", votoMax=" + votoMax + ", usaStessoPlugin=" + usaStessoPlugin + '}');
    }

    @Override
    public void save() throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        dbBroker.SollecitazioneDB.save(this);
    }
    
    @Override
    public void saveMod()
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        dbBroker.SollecitazioneDB.saveMod(this);
    }
    
    @Override
    public void load() throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        dbBroker.SollecitazioneDB.load(this);
        if (GuiController.DEBUG) {
            System.out.println(">>SOLLECITAZIONE.jv:40<< soll:" + this);
        }
    }
}
