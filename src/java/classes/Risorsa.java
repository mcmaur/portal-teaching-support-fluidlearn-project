package classes;

import GUIcontroller.GuiController;
import java.sql.SQLException;

public class Risorsa {

    private Integer id;
    private String url, nome;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Risorsa{" + "id=" + id + ", url=" + url + ", nome=" + nome + '}';
    }

    public void load() 
            throws InstantiationException, ClassNotFoundException, SQLException, IllegalAccessException{
        dbBroker.RisorsaDB.load(this);
        if (GuiController.DEBUG) {
            System.out.println(">>RISORSA.jv:43<< risorsa:" + this);
        }
    }
}
