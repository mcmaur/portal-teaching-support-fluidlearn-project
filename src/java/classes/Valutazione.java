package classes;

import GUIcontroller.GuiController;
import java.sql.SQLException;

public class Valutazione {
    Integer id,risposta_id,id_corso;
    String esaminatore,autore_risp,nota,voto, nome_corso;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRisposta_id() {
        return risposta_id;
    }

    public void setRisposta_id(Integer risposta_id) {
        this.risposta_id = risposta_id;
    }

    public Integer getId_corso() {
        return id_corso;
    }

    public void setId_corso(Integer id_corso) {
        this.id_corso = id_corso;
    }

    public String getEsaminatore() {
        return esaminatore;
    }

    public void setEsaminatore(String esaminatore) {
        this.esaminatore = esaminatore;
    }

    public String getAutore_risp() {
        return autore_risp;
    }

    public void setAutore_risp(String autore_risp) {
        this.autore_risp = autore_risp;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getVoto() {
        return voto;
    }

    public void setVoto(String voto) {
        this.voto = voto;
    }

    public String getNome_corso() {
        return nome_corso;
    }

    public void setNome_corso(String nome_corso) {
        this.nome_corso = nome_corso;
    }

    @Override
    public String toString() {
        return "Valutazione{" + "id=" + id + ", risposta_id=" + risposta_id + ", id_corso=" + id_corso + ", esaminatore=" + esaminatore + ", autore_risp=" + autore_risp + ", nota=" + nota + ", voto=" + voto + ", nome_corso=" + nome_corso + '}';
    }

    public void salva() throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException{
        if(GuiController.DEBUG) System.out.println("<<VALUTAZIONE.jv:64>> valutazione: "+this);
        dbBroker.ValutazioneDB.salva(this);
    }
}