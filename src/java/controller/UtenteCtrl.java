package controller;

import static GUIcontroller.GuiController.DEBUG;
import dbBroker.UtenteDB;
import classes.Utente;

public class UtenteCtrl {

    /**
     * Requests for course's info
     *
     * @param CourseName course's name
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public Utente retrievePersonalInfo(String UserName)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Utente user = dbBroker.UtenteDB.retrievePersonalInfo(UserName);
        if (DEBUG) {
            System.out.println(">>USERCONTROLLER:18>>:PrintoUser: " + user);
        }
        return user;
    }

    /**
     * Requests if the user is in the database
     *
     * @param CourseName course's name
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public boolean checkLogin(String UserName, String password)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        return dbBroker.UtenteDB.checkLogin(UserName, password);
    }

    /**
     * Add an user to the Users
     *
     * @param usr
     * @param pwd
     * @param email
     * @return true if adding new user goes right , false otherwise
     */
    static public boolean addUser(String usr, String pwd, String email)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        return UtenteDB.addUser(usr, pwd, email);
    }
}
