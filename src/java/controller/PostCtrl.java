package controller;

import java.util.List;
import classes.partedinamica.Post;
import java.sql.SQLException;

public class PostCtrl {

    /**
     * Processes requests for all post (by course)
     *
     * @param coursename course's name
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public List<Post> recuperaPostDatoNomeCorso(String courseName) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        List<Post> list = dbBroker.PostDB.retrieveAllPostByCourse(courseName);
        return list;
    }

    /**
     * Processes requests for all post (by course)
     *
     * @param username name of the user
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public List<Post> recuperaPostDatoNomeUtente(String username) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        List<Post> list = dbBroker.PostDB.retrieveAllPostByUser(username);
        return list;
    }

    /**
     * Processes requests for add post
     *
     * @param username name of the user
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public void aggiungiPost(Object attore, Object data, Object id_corso, Object id_risorsa, Object id_uda, Object isBozza, Object nome_corso, Object ora, Object plugin, Object titolo, Object visibilita, Object testo)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Post pst = new Post();
        if (attore != null) {
            pst.setAttore((String) attore);
        }
        if (data != null) {
            pst.setData((String) data);
        }
        pst.setId(null);
        pst.setId_artefatto(null);
        if (id_corso != null) {
            pst.setId_corso(new Integer((String) id_corso));
        }
        if (id_risorsa != null) {
            pst.setId_risorsa(new Integer((String) id_risorsa));
        }
        if (id_uda != null) {
            pst.setId_uda(new Integer((String) id_uda));
        }
        if (nome_corso != null) {
            pst.setNome_corso((String) nome_corso);
        }
        if (ora != null) {
            pst.setOra((String) ora);
        }
        if (plugin != null) {
            pst.setPlugin((String) plugin);
        }
        if (testo != null) {
            pst.setTesto((String) testo);
        }
        if (titolo != null) {
            pst.setTitolo((String) titolo);
        }
        if (visibilita != null) {
            pst.setVisibilita((String) visibilita);
        }
        if (isBozza == null) {
            pst.setIsBozza(false);
        } else {
            pst.setIsBozza(true);
        }
        pst.save();
    }

    /**
     * Processes requests for create post
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public Post creaPost(Object attore, Object data, Object id_corso, Object id_risorsa, Object id_uda, Object isBozza, Object nome_corso, Object ora, Object plugin, Object titolo, Object visibilita, Object testo)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Post pst = new Post();
        if (attore != null) {
            pst.setAttore((String) attore);
        }
        if (data != null) {
            pst.setData((String) data);
        }
        pst.setId(null);
        if (id_corso != null) {
            pst.setId_corso(new Integer((String) id_corso));
        }
        if (id_risorsa != null) {
            pst.setId_risorsa(new Integer((String) id_risorsa));
        }
        if (id_uda != null) {
            pst.setId_uda(new Integer((String) id_uda));
        }
        if (nome_corso != null) {
            pst.setNome_corso((String) nome_corso);
        }
        if (ora != null) {
            pst.setOra((String) ora);
        }
        if (plugin != null) {
            pst.setPlugin((String) plugin);
        }
        if (testo != null) {
            pst.setTesto((String) testo);
        }
        if (titolo != null) {
            pst.setTitolo((String) titolo);
        }
        if (visibilita != null) {
            pst.setVisibilita((String) visibilita);
        }
        if (isBozza == null) {
            pst.setIsBozza(false);
        } else {
            pst.setIsBozza(true);
        }
        return pst;
    }

    /**
     * Processes requests for delete post
     *
     * @param idPost name of the user
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public void eliminaPost(String idPost) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        dbBroker.PostDB.deletePost(idPost);
    }

    /**
     * Processes requests for save a draft of a post
     *
     * @param idPost name of the user
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public void transformaBozzaInPost(String idPost) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        dbBroker.PostDB.savePostDraft(idPost);
    }

    /**
     * Processes requests for loading an existing post
     *
     * @param idPost name of the user
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static Post caricaPost(Object id) throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        Post pst = new Post();
        pst.setId(new Integer((String) id));
        pst.load();
        return pst;
    }

    /**
     * Processes requests for saving a modified post
     *
     * @param username name of the user
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public void salvaPostModificato(Object id, Object attore, Object data, Object id_corso, Object id_risorsa, Object id_uda, Object isBozza, Object nome_corso, Object ora, Object plugin, Object titolo, Object visibilita, Object testo)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Post pst = new Post();
        if (id != null) {
            pst.setId(new Integer((String) id));
        }
        if (attore != null) {
            pst.setAttore((String) attore);
        }
        if (data != null) {
            pst.setData((String) data);
        }
        if (id_corso != null) {
            pst.setId_corso(new Integer((String) id_corso));
        }
        if (id_risorsa != null) {
            pst.setId_risorsa(new Integer((String) id_risorsa));
        }
        if (id_uda != null) {
            pst.setId_uda(new Integer((String) id_uda));
        }
        if (nome_corso != null) {
            pst.setNome_corso((String) nome_corso);
        }
        if (ora != null) {
            pst.setOra((String) ora);
        }
        if (plugin != null) {
            pst.setPlugin((String) plugin);
        }
        if (testo != null) {
            pst.setTesto((String) testo);
        }
        if (titolo != null) {
            pst.setTitolo((String) titolo);
        }
        if (visibilita != null) {
            pst.setVisibilita((String) visibilita);
        }
        if (isBozza == null) {
            pst.setIsBozza(false);
        } else if (((String) isBozza).equals("on")) {
            pst.setIsBozza(true);
        }
        pst.saveMod();
    }

     /**
     * Processes requests for transforming a post to draft
     *
     * @param idPost name of the user
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static void transformaInBozza(String idPost) 
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        dbBroker.PostDB.transformaInBozza(idPost);
    }
}