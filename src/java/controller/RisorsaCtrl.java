package controller;

import classes.Risorsa;
import java.sql.SQLException;

public class RisorsaCtrl {

    /**
     * Processes requests for loading an existing post
     *
     * @param idPost name of the user
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static Risorsa load(String id)
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        Risorsa ris = new Risorsa();
        ris.setId(Integer.parseInt(id));
        ris.load();
        return ris;
    }

}
