package controller;

import classes.Valutazione;
import java.sql.SQLException;
import java.util.List;

public class ValutazioneCtrl {

    public static void salvaValutazione(Object id_risposta, Object voto, Object nota, Object esaminatore, Object autore_risposta,Object id_corso,Object nome_corso) 
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Valutazione val = new Valutazione();
        val.setId(null);
        if(id_risposta!=null)
            val.setRisposta_id(new Integer((String)id_risposta));
        if(esaminatore!=null)
            val.setEsaminatore((String)esaminatore);
        if(autore_risposta!=null)
            val.setAutore_risp((String)autore_risposta);
        if(nota!=null)
            val.setNota((String)nota);
        if(voto!=null)
            val.setVoto((String)voto);
        if(id_corso!=null)
            val.setId_corso(new Integer((String)id_corso));
        if(nome_corso!=null)
            val.setNome_corso((String)nome_corso);
        val.salva();
    }

    public static List<Valutazione> recuperaTutteValutazioni() 
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        return dbBroker.ValutazioneDB.recuperaTutteValutazioni();
    }
    
}
