package controller;

import GUIcontroller.GuiController;
import classes.partedinamica.Risposta;
import java.sql.SQLException;
import java.util.List;

public class RispostaCtrl {

    /**
     * Richiesta di creare un risposta
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static Risposta creaRisposta(Object id_sollecitazione,Object attore) {
        Risposta ris = new Risposta();
        ris.setId(null);//inizio contrib
        ris.setId_risorsa(null);
        ris.setAttore((String)attore);
        ris.setTesto(null);//fine contrib
        ris.setId_contributo(new Integer((String)id_sollecitazione));//inizio commento
        ris.setPlugin(null);
        ris.setId_valutazione(null);
        ris.setId_artefatto(null);
        if (GuiController.DEBUG) {
            System.out.println(">>RISPOSTACTRL:24<< risposta: " + ris);
        }
        return ris;
    }    
    
    
    public static void aggiungiRisposta(Object attore, Object testo, Object id_contributo, Object plugin, Object id_valutazione, Object id_artefatto, Object id_risorsa) 
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        Risposta ris = new Risposta();
        ris.setId(null);//inizio contrib
        if(id_risorsa!=null)
            ris.setId_risorsa(new Integer((String)id_risorsa));
        ris.setAttore((String)attore);
        if(testo!=null)
            ris.setTesto((String)testo);//fine contrib
        if(id_contributo!=null)
            ris.setId_contributo(new Integer((String)id_contributo));//inizio commento
        ris.setPlugin(null);
        ris.setId_valutazione(null);
        ris.setId_artefatto(null);
        if (GuiController.DEBUG) {
            System.out.println(">>RISPOSTACTRL:24<< risposta: " + ris);
        }
        ris.salva();
    }

    public static List <Risposta> recuperaRisposteDataSollecitazione(String id_sollecitazione) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        List <Risposta> list = dbBroker.RispostaDB.recuperaRisposte(id_sollecitazione);
        return list;
    }
}
