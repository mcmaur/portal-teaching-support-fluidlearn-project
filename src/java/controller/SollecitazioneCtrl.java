package controller;

import GUIcontroller.GuiController;
import static GUIcontroller.GuiController.DEBUG;
import classes.partedinamica.Sollecitazione;
import java.sql.SQLException;
import java.util.List;

public class SollecitazioneCtrl {

    /**
     * Processes requests for create post
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public Sollecitazione creaSollecitazione(Object id_corso, Object nome_corso)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Sollecitazione soll = new Sollecitazione();
        if (nome_corso != null) {
            soll.setNome_corso((String) nome_corso);
        }
        soll.setIsBozza(false);
        if (id_corso != null) {
            soll.setId_corso(new Integer((String) id_corso));
        }
        if (DEBUG) {
            System.out.println(">>SOLLECITAZIONECTRL:34<< sollecitazione: " + soll);
        }
        return soll;
    }

    /**
     * Processes requests for create post
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public void aggiungiSollecitazione(Object id_risorsa, Object attore, Object testo, Object titolo,
            Object data, Object ora, Object visibilita, Object nome_corso, Object isBozza, Object id_corso, Object id_uda,
            Object dataScadenza,Object votoMax,Object visibilitaRisp) throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        Sollecitazione soll = new Sollecitazione();
        soll.setId(null);//inizio contrib
        if (id_risorsa != null) {
            soll.setId_risorsa(new Integer((String) id_risorsa));
        }
        if (attore != null) {
            soll.setAttore((String) attore);
        }
        if (testo != null) {
            soll.setTesto((String) testo);//fine contrib
        }
        if (titolo != null) {
            soll.setTitolo((String) titolo);//inizio post
        }
        if (data != null) {
            soll.setData((String) data);
        }
        if (ora != null) {
            soll.setOra((String) ora);
        }
        soll.setPlugin(null);
        if (visibilita != null) {
            soll.setVisibilita((String) visibilita);
        }
        if (nome_corso != null) {
            soll.setNome_corso((String) nome_corso);
        }
        if (isBozza == null) {
            soll.setIsBozza(false);
        } else {
            soll.setIsBozza(true);
        }
        if (id_corso != null) {
            soll.setId_corso(new Integer((String) id_corso));
        }
        if (id_uda != null) {
            soll.setId_uda(new Integer((String) id_uda));
        }
        soll.setId_artefatto(null);//fine post
        if (dataScadenza != null) {
            soll.setDataScadenza((String) dataScadenza);
        }
        soll.setUsaStessoPlugin(false);
        soll.setVotoMax((String)votoMax);
        soll.setVisibilitaRisposte((String)visibilitaRisp);
        if (DEBUG) {
            System.out.println(">>SOLLECITAZIONECTRL:34<< sollecitazione: " + soll);
        }
        soll.save();
    }

    /**
     * Processes requests for all sollecitazioni
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static List<Sollecitazione> recuperaTutteSollecitazione(Object corso_id) 
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        if(corso_id!=null)
            return dbBroker.SollecitazioneDB.recuperaTutteSollecitazione(new Integer((String)corso_id));
        else
            return dbBroker.SollecitazioneDB.recuperaTutteSollecitazione(null);
    }
    
    /**
     * Processes requests for loading an existing post
     *
     * @param idPost name of the user
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static Sollecitazione caricaSollecitazione(Object id) 
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        Sollecitazione soll = new Sollecitazione();
        soll.setId(new Integer((String) id));
        soll.load();
        return soll;
    }
    
    /**
     * Processes requests for saving a modified post
     *
     * @param username name of the user
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public void salvaSollecitazioneModificata(Object id, Object attore, Object data, Object id_corso, Object id_risorsa, Object id_uda, Object isBozza, Object nome_corso, Object ora, Object plugin, Object titolo, Object visibilita, Object testo, Object dataScadenza)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Sollecitazione soll = new Sollecitazione();
        if (id != null) {
            soll.setId(new Integer((String) id));
        }
        if (attore != null) {
            soll.setAttore((String) attore);
        }
        if (data != null) {
            soll.setData((String) data);
        }
        if (id_corso != null) {
            soll.setId_corso(new Integer((String) id_corso));
        }
        if (id_risorsa != null) {
            soll.setId_risorsa(new Integer((String) id_risorsa));
        }
        if (id_uda != null) {
            soll.setId_uda(new Integer((String) id_uda));
        }
        if (nome_corso != null) {
            soll.setNome_corso((String) nome_corso);
        }
        if (ora != null) {
            soll.setOra((String) ora);
        }
        if (plugin != null) {
            soll.setPlugin((String) plugin);
        }
        if (testo != null) {
            soll.setTesto((String) testo);
        }
        if (titolo != null) {
            soll.setTitolo((String) titolo);
        }
        if (visibilita != null) {
            soll.setVisibilita((String) visibilita);
        }
        if (isBozza == null) {
            soll.setIsBozza(false);
        } else if (((String) isBozza).equals("on")) {
            soll.setIsBozza(true);
        }
        if (dataScadenza != null) {
            soll.setDataScadenza((String)dataScadenza);
        }
        if(GuiController.DEBUG) System.out.println("<<SOLLECITAZIONECTRL:191>> sollMod: "+soll);
        soll.saveMod();
    }

    public static void eliminaSollecitazione(Object id) 
            throws ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
        dbBroker.SollecitazioneDB.eliminaSollecitazione(new Integer((String)id));
    }
}
