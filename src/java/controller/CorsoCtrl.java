package controller;

import static GUIcontroller.GuiController.DEBUG;
import java.util.List;
import classes.Corso;

public class CorsoCtrl {
    
    /**
     * Requests for course's info
     *
     * @param CourseName course's name
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public Corso retrieveCourseInfo(String CourseName) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        Corso course = dbBroker.CorsoDB.retrieveCourseInfo(CourseName);
        if(DEBUG) System.out.println(">>COURSECONTROLLER:19>>:PrintoCorso: "+course);
        return course;
    }

    /**
     * Requests for list of all courses
     * 
     *@throws ClassNotFoundException
     *@throws InstantiationException
     *@throws IllegalAccessException
     */
    static public List <Corso> retrieveListAllCourses() throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        List <Corso> courses = dbBroker.CorsoDB.retrieveAllCourses();
        if(DEBUG) System.out.println(">>COURSECONTROLLER:32>>:PrintoListaCorsi: "+courses);
        return courses;
    }

    /**
     * Requests for list of courses of specified year
     * 
     *@throws ClassNotFoundException
     *@throws InstantiationException
     *@throws IllegalAccessException
     */
    static public List <Corso> retrieveListCoursesByYear(String year) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        List <Corso> courses = dbBroker.CorsoDB.retrieveCoursesByYear(year);
        if(DEBUG) System.out.println(">>COURSECONTROLLER:45>>:PrintoListaCorsiPerAnnoSpecificato: "+courses);
        return courses;
    }
    
}