package controller;

import java.util.List;
import classes.partedinamica.Commento;
import java.sql.SQLException;

public class CommentoCtrl {

    /**
     * Requests for adding new comment
     *
     * @param CourseName course's name
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public void addComment(Object attore, Object testo, Object risorsa, Object post)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Commento cmt = new Commento();
        cmt.setAttore((String) attore);
        cmt.setId(null);
        cmt.setId_contributo(new Integer((String) post));
        cmt.setId_risorsa(new Integer((String) risorsa));
        cmt.setTesto((String) testo);
        cmt.save();
    }

    /**
     * Retrieve all comments
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public List<Commento> retrieveAllComments() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        return dbBroker.CommentoDB.retrieveAllComments();
    }

    /**
     * Delete all comments that refers to give post_id
     *
     * @param post_id
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    static public void deleteCommentsByPostID(String post_id) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        dbBroker.CommentoDB.deleteCommentsByPostID(post_id);
    }

    /**
     * Elimina commento
     *
     * @param id
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static void eliminaCommento(Object id) 
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        dbBroker.CommentoDB.deleteCommentsByID(new Integer((String)id));    
    }
}
