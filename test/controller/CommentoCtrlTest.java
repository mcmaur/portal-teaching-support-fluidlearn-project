package controller;

import classes.partedinamica.Commento;
import java.sql.SQLException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class CommentoCtrlTest {
    static int maxId;
    
    public CommentoCtrlTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() 
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        dbBroker.CommentoDB.deleteCommentsByID(maxId);
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addComment method, of class CommentoCtrl.
     */
    @Test
    public void testAddComment() throws Exception {
        System.out.println("addComment");
        Object attore = "pippo.franco";
        Object testo = "Tanti saluti da pippo franco";
        Object risorsa = "19";
        Object post = "10";
        CommentoCtrl.addComment(attore, testo, risorsa, post);
        maxId = dbBroker.CommentoDB.getMaximumId();
        Commento result = new Commento();
        result.setId(maxId);
        result.load();
        assertEquals(new Integer((String)risorsa), result.getId_risorsa());
        assertEquals(attore, result.getAttore());
        assertEquals(testo, result.getTesto());
        assertEquals(new Integer((String)post), result.getId_contributo());
    }

    /**
     * Test of deleteCommentsByPostID method, of class CommentoCtrl.
     */
    @Test
    public void testDeleteCommentsByPostID() throws Exception {
        System.out.println("deleteCommentsByPostID");
        Object attore = "paolo.franco";
        Object testo = "si sono qui";
        Object risorsa = "19";
        Object post = "12345";
        CommentoCtrl.addComment(attore, testo, risorsa, post);
        int maxIdPreDel = dbBroker.CommentoDB.getMaximumId();
        String post_id = "12345";
        CommentoCtrl.deleteCommentsByPostID(post_id);
        int maxIdPostDel = dbBroker.CommentoDB.getMaximumId();
        boolean b = maxIdPreDel == maxIdPostDel;
        assertEquals(b, false);
    }
}