package classes.partedinamica;

import java.sql.SQLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class CommentoTest {
    static int maxId;
    
    public CommentoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() 
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        dbBroker.CommentoDB.deleteCommentsByID(maxId);
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId_contributo method, of class Commento.
     */
    @Test
    public void testGetId_contributo() {
        System.out.println("getId_contributo");
        Commento instance = new Commento();
        instance.setId_contributo(1);
        Integer expResult = 1;
        Integer result = instance.getId_contributo();
        assertEquals(expResult, result);
    }

    /**
     * Test of save method, of class Commento.
     */
    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        Commento instance = new Commento();
        instance.setId_contributo(2);
        instance.setAttore("gianfi.gigi");
        instance.setTesto("auguri");
        instance.setId_risorsa(64);
        instance.save();
        maxId = dbBroker.CommentoDB.getMaximumId();
        Commento result = new Commento();
        result.setId(maxId);
        result.load();
        assertEquals(instance.getId_risorsa(), result.getId_risorsa());
        assertEquals(instance.getAttore(), result.getAttore());
        assertEquals(instance.getTesto(), result.getTesto());
        assertEquals(instance.getId_contributo(), result.getId_contributo());
    }

}